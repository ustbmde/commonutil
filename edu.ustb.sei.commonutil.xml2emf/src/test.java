import java.io.IOException;
import java.lang.reflect.Proxy;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import edu.ustb.sei.commonutil.eXML.EXMLFactory;
import edu.ustb.sei.commonutil.eXML.XMLDocument;
import edu.ustb.sei.commonutil.eXML.util.ConversionHelper;
public class test {

	public static void main(String[] args) {
		
		DocumentBuilder newDocumentBuilder;
		try {
			newDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
						
			Document d = newDocumentBuilder.newDocument();
			
			Element e1 = d.createElementNS("eee","aa:dd");
			Element e2 = d.createElementNS("eee","aa:dd1");
			Element e3 = d.createElement("dd2");
			d.appendChild(e1);
			e1.appendChild(e2);
			e1.appendChild(e3);
			e2.setTextContent("hello");
			e3.setTextContent("world");
			
			XMLDocument xd = EXMLFactory.eINSTANCE.createXMLDocument();
			
			xd.setDocument(d);
			
			xd.forward();
			
			xd.getChildNodes().get(0).getChildNodes().get(0).setNodeName("ttt");
			xd.backward();
			
			System.out.println(xd);
			
			
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}