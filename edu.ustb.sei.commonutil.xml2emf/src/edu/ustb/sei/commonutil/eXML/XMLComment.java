/**
 */
package edu.ustb.sei.commonutil.eXML;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XML Comment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLComment()
 * @model
 * @generated
 */
public interface XMLComment extends XMLCharacterData {
} // XMLComment
