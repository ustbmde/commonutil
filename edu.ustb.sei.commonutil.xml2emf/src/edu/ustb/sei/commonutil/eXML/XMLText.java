/**
 */
package edu.ustb.sei.commonutil.eXML;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XML Text</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLText()
 * @model
 * @generated
 */
public interface XMLText extends XMLCharacterData {
} // XMLText
