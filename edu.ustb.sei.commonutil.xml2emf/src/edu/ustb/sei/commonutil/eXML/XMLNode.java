/**
 */
package edu.ustb.sei.commonutil.eXML;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XML Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLNode#getBaseURI <em>Base URI</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLNode#getChildNodes <em>Child Nodes</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLNode#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLNode#getNamespaceURI <em>Namespace URI</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLNode#getNodeName <em>Node Name</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLNode#getNodeType <em>Node Type</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLNode#getParentNode <em>Parent Node</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLNode()
 * @model abstract="true"
 * @generated
 */
public interface XMLNode extends EObject {
	/**
	 * Returns the value of the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base URI</em>' attribute.
	 * @see #setBaseURI(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLNode_BaseURI()
	 * @model
	 * @generated
	 */
	String getBaseURI();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getBaseURI <em>Base URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base URI</em>' attribute.
	 * @see #getBaseURI()
	 * @generated
	 */
	void setBaseURI(String value);

	/**
	 * Returns the value of the '<em><b>Child Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.commonutil.eXML.XMLNode}.
	 * It is bidirectional and its opposite is '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getParentNode <em>Parent Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Nodes</em>' containment reference list.
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLNode_ChildNodes()
	 * @see edu.ustb.sei.commonutil.eXML.XMLNode#getParentNode
	 * @model opposite="parentNode" containment="true"
	 * @generated
	 */
	EList<XMLNode> getChildNodes();

	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.commonutil.eXML.XMLAttr}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' containment reference list.
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLNode_Attributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<XMLAttr> getAttributes();

	/**
	 * Returns the value of the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Namespace URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Namespace URI</em>' attribute.
	 * @see #setNamespaceURI(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLNode_NamespaceURI()
	 * @model
	 * @generated
	 */
	String getNamespaceURI();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getNamespaceURI <em>Namespace URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Namespace URI</em>' attribute.
	 * @see #getNamespaceURI()
	 * @generated
	 */
	void setNamespaceURI(String value);

	/**
	 * Returns the value of the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Name</em>' attribute.
	 * @see #setNodeName(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLNode_NodeName()
	 * @model
	 * @generated
	 */
	String getNodeName();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getNodeName <em>Node Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Name</em>' attribute.
	 * @see #getNodeName()
	 * @generated
	 */
	void setNodeName(String value);

	/**
	 * Returns the value of the '<em><b>Node Type</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.ustb.sei.commonutil.eXML.NodeType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Type</em>' attribute.
	 * @see edu.ustb.sei.commonutil.eXML.NodeType
	 * @see #setNodeType(NodeType)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLNode_NodeType()
	 * @model
	 * @generated
	 */
	NodeType getNodeType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getNodeType <em>Node Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Type</em>' attribute.
	 * @see edu.ustb.sei.commonutil.eXML.NodeType
	 * @see #getNodeType()
	 * @generated
	 */
	void setNodeType(NodeType value);

	/**
	 * Returns the value of the '<em><b>Parent Node</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getChildNodes <em>Child Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Node</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Node</em>' container reference.
	 * @see #setParentNode(XMLNode)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLNode_ParentNode()
	 * @see edu.ustb.sei.commonutil.eXML.XMLNode#getChildNodes
	 * @model opposite="childNodes" transient="false"
	 * @generated
	 */
	XMLNode getParentNode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getParentNode <em>Parent Node</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Node</em>' container reference.
	 * @see #getParentNode()
	 * @generated
	 */
	void setParentNode(XMLNode value);

} // XMLNode
