/**
 */
package edu.ustb.sei.commonutil.eXML;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XMLCDATA Section</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLCDATASection()
 * @model
 * @generated
 */
public interface XMLCDATASection extends XMLText {
} // XMLCDATASection
