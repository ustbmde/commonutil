/**
 */
package edu.ustb.sei.commonutil.eXML.util;

import edu.ustb.sei.commonutil.eXML.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage
 * @generated
 */
public class EXMLSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static EXMLPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EXMLSwitch() {
		if (modelPackage == null) {
			modelPackage = EXMLPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case EXMLPackage.XML_NODE: {
				XMLNode xmlNode = (XMLNode)theEObject;
				T result = caseXMLNode(xmlNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XML_ATTR: {
				XMLAttr xmlAttr = (XMLAttr)theEObject;
				T result = caseXMLAttr(xmlAttr);
				if (result == null) result = caseXMLNode(xmlAttr);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XMLCDATA_SECTION: {
				XMLCDATASection xmlcdataSection = (XMLCDATASection)theEObject;
				T result = caseXMLCDATASection(xmlcdataSection);
				if (result == null) result = caseXMLText(xmlcdataSection);
				if (result == null) result = caseXMLCharacterData(xmlcdataSection);
				if (result == null) result = caseXMLNode(xmlcdataSection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XML_CHARACTER_DATA: {
				XMLCharacterData xmlCharacterData = (XMLCharacterData)theEObject;
				T result = caseXMLCharacterData(xmlCharacterData);
				if (result == null) result = caseXMLNode(xmlCharacterData);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XML_COMMENT: {
				XMLComment xmlComment = (XMLComment)theEObject;
				T result = caseXMLComment(xmlComment);
				if (result == null) result = caseXMLCharacterData(xmlComment);
				if (result == null) result = caseXMLNode(xmlComment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XML_DOCUMENT: {
				XMLDocument xmlDocument = (XMLDocument)theEObject;
				T result = caseXMLDocument(xmlDocument);
				if (result == null) result = caseXMLNode(xmlDocument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XML_DOCUMENT_FRAGMENT: {
				XMLDocumentFragment xmlDocumentFragment = (XMLDocumentFragment)theEObject;
				T result = caseXMLDocumentFragment(xmlDocumentFragment);
				if (result == null) result = caseXMLNode(xmlDocumentFragment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XML_DOCUMENT_TYPE: {
				XMLDocumentType xmlDocumentType = (XMLDocumentType)theEObject;
				T result = caseXMLDocumentType(xmlDocumentType);
				if (result == null) result = caseXMLNode(xmlDocumentType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XML_ELEMENT: {
				XMLElement xmlElement = (XMLElement)theEObject;
				T result = caseXMLElement(xmlElement);
				if (result == null) result = caseXMLNode(xmlElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XML_ENTITY: {
				XMLEntity xmlEntity = (XMLEntity)theEObject;
				T result = caseXMLEntity(xmlEntity);
				if (result == null) result = caseXMLNode(xmlEntity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XML_ENTITY_REFERENCE: {
				XMLEntityReference xmlEntityReference = (XMLEntityReference)theEObject;
				T result = caseXMLEntityReference(xmlEntityReference);
				if (result == null) result = caseXMLNode(xmlEntityReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XML_NOTATION: {
				XMLNotation xmlNotation = (XMLNotation)theEObject;
				T result = caseXMLNotation(xmlNotation);
				if (result == null) result = caseXMLNode(xmlNotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XML_PROCESSING_INSTRUCTION: {
				XMLProcessingInstruction xmlProcessingInstruction = (XMLProcessingInstruction)theEObject;
				T result = caseXMLProcessingInstruction(xmlProcessingInstruction);
				if (result == null) result = caseXMLNode(xmlProcessingInstruction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case EXMLPackage.XML_TEXT: {
				XMLText xmlText = (XMLText)theEObject;
				T result = caseXMLText(xmlText);
				if (result == null) result = caseXMLCharacterData(xmlText);
				if (result == null) result = caseXMLNode(xmlText);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLNode(XMLNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Attr</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Attr</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLAttr(XMLAttr object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XMLCDATA Section</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XMLCDATA Section</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLCDATASection(XMLCDATASection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Character Data</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Character Data</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLCharacterData(XMLCharacterData object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Comment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Comment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLComment(XMLComment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Document</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Document</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLDocument(XMLDocument object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Document Fragment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Document Fragment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLDocumentFragment(XMLDocumentFragment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Document Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Document Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLDocumentType(XMLDocumentType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLElement(XMLElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLEntity(XMLEntity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Entity Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Entity Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLEntityReference(XMLEntityReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Notation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Notation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLNotation(XMLNotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Processing Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Processing Instruction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLProcessingInstruction(XMLProcessingInstruction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Text</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Text</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLText(XMLText object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //EXMLSwitch
