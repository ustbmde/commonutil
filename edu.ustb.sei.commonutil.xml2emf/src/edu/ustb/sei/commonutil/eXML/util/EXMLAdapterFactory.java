/**
 */
package edu.ustb.sei.commonutil.eXML.util;

import edu.ustb.sei.commonutil.eXML.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage
 * @generated
 */
public class EXMLAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static EXMLPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EXMLAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = EXMLPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EXMLSwitch<Adapter> modelSwitch =
		new EXMLSwitch<Adapter>() {
			@Override
			public Adapter caseXMLNode(XMLNode object) {
				return createXMLNodeAdapter();
			}
			@Override
			public Adapter caseXMLAttr(XMLAttr object) {
				return createXMLAttrAdapter();
			}
			@Override
			public Adapter caseXMLCDATASection(XMLCDATASection object) {
				return createXMLCDATASectionAdapter();
			}
			@Override
			public Adapter caseXMLCharacterData(XMLCharacterData object) {
				return createXMLCharacterDataAdapter();
			}
			@Override
			public Adapter caseXMLComment(XMLComment object) {
				return createXMLCommentAdapter();
			}
			@Override
			public Adapter caseXMLDocument(XMLDocument object) {
				return createXMLDocumentAdapter();
			}
			@Override
			public Adapter caseXMLDocumentFragment(XMLDocumentFragment object) {
				return createXMLDocumentFragmentAdapter();
			}
			@Override
			public Adapter caseXMLDocumentType(XMLDocumentType object) {
				return createXMLDocumentTypeAdapter();
			}
			@Override
			public Adapter caseXMLElement(XMLElement object) {
				return createXMLElementAdapter();
			}
			@Override
			public Adapter caseXMLEntity(XMLEntity object) {
				return createXMLEntityAdapter();
			}
			@Override
			public Adapter caseXMLEntityReference(XMLEntityReference object) {
				return createXMLEntityReferenceAdapter();
			}
			@Override
			public Adapter caseXMLNotation(XMLNotation object) {
				return createXMLNotationAdapter();
			}
			@Override
			public Adapter caseXMLProcessingInstruction(XMLProcessingInstruction object) {
				return createXMLProcessingInstructionAdapter();
			}
			@Override
			public Adapter caseXMLText(XMLText object) {
				return createXMLTextAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLNode <em>XML Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLNode
	 * @generated
	 */
	public Adapter createXMLNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLAttr <em>XML Attr</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLAttr
	 * @generated
	 */
	public Adapter createXMLAttrAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLCDATASection <em>XMLCDATA Section</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLCDATASection
	 * @generated
	 */
	public Adapter createXMLCDATASectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLCharacterData <em>XML Character Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLCharacterData
	 * @generated
	 */
	public Adapter createXMLCharacterDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLComment <em>XML Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLComment
	 * @generated
	 */
	public Adapter createXMLCommentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLDocument <em>XML Document</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocument
	 * @generated
	 */
	public Adapter createXMLDocumentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLDocumentFragment <em>XML Document Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocumentFragment
	 * @generated
	 */
	public Adapter createXMLDocumentFragmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLDocumentType <em>XML Document Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocumentType
	 * @generated
	 */
	public Adapter createXMLDocumentTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLElement <em>XML Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLElement
	 * @generated
	 */
	public Adapter createXMLElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLEntity <em>XML Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLEntity
	 * @generated
	 */
	public Adapter createXMLEntityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLEntityReference <em>XML Entity Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLEntityReference
	 * @generated
	 */
	public Adapter createXMLEntityReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLNotation <em>XML Notation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLNotation
	 * @generated
	 */
	public Adapter createXMLNotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction <em>XML Processing Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction
	 * @generated
	 */
	public Adapter createXMLProcessingInstructionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.commonutil.eXML.XMLText <em>XML Text</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.commonutil.eXML.XMLText
	 * @generated
	 */
	public Adapter createXMLTextAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //EXMLAdapterFactory
