package edu.ustb.sei.commonutil.eXML.util;

import java.util.ArrayList;

import org.eclipse.emf.common.util.EList;
import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

import edu.ustb.sei.commonutil.eXML.EXMLFactory;
import edu.ustb.sei.commonutil.eXML.NodeType;
import edu.ustb.sei.commonutil.eXML.XMLAttr;
import edu.ustb.sei.commonutil.eXML.XMLCDATASection;
import edu.ustb.sei.commonutil.eXML.XMLComment;
import edu.ustb.sei.commonutil.eXML.XMLDocument;
import edu.ustb.sei.commonutil.eXML.XMLDocumentType;
import edu.ustb.sei.commonutil.eXML.XMLElement;
import edu.ustb.sei.commonutil.eXML.XMLNode;
import edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction;
import edu.ustb.sei.commonutil.eXML.XMLText;
import edu.ustb.sei.commonutil.util.BidirectionalMap;

public class ConversionHelper {

	public static void forwardDocument(Document d, XMLDocument ed,BidirectionalMap<Node,XMLNode> map) {
		forwardNode(d,ed,map);
		
		ed.setDocumentURI(d.getDocumentURI());
		ed.setInputEncoding(d.getInputEncoding());
		ed.setStrictErrorChecking(d.getStrictErrorChecking());
		ed.setXmlEncoding(d.getXmlEncoding());
		ed.setXmlStandalone(d.getXmlStandalone());
		ed.setXmlVersion(d.getXmlVersion());
	}
	
	public static void backwardDocument(XMLDocument ed,Document d,BidirectionalMap<Node,XMLNode> map) {
		backwardNode(ed,d,map);
		
		d.setDocumentURI(ed.getDocumentURI());
		//d.setInputEncoding(ed.getInputEncoding());
		d.setStrictErrorChecking(ed.isStrictErrorChecking());
		//d.setXmlEncoding(ed.getXmlEncoding());
		d.setXmlStandalone(ed.isXmlStandalone());
		d.setXmlVersion(ed.getXmlVersion());
	}

	static private void forwardNode(Node n, XMLNode en,BidirectionalMap<Node,XMLNode> map) {
		en.setBaseURI(n.getBaseURI());
		en.setNamespaceURI(n.getNamespaceURI());
		en.setNodeName(n.getNodeName());
		forwardNodeType(n,en);
		
		
		
		NodeList list = n.getChildNodes();
		{
			int size = list.getLength();
			for(int i=0;i<size;i++) {
				Node cn = list.item(i);
				XMLNode ecn = map.forward(cn);
				
				if(ecn==null) {
					ecn = forwardInstance(cn, map);
				}
				
				if(en.getChildNodes().contains(ecn)==false)
					en.getChildNodes().add(ecn);
				
				forward(cn,ecn,map);
			}
			
			ArrayList<XMLNode> del = new ArrayList<XMLNode>(); 
			
			for(XMLNode ea : en.getChildNodes()) {
				Node a = (Node)map.backward(ea);
				if(a==null
						|| a.getParentNode()==null) {
					del.add(ea);
					map.removeBackward(ea);
				} else if(a.getParentNode()!=n) {
					del.add(ea);
				}
			}
			
			en.getChildNodes().removeAll(del);
		}
	}
	
	static private boolean compareString(String a,String b) {
		if(a==b) return true;
		if(a!=null) return a.equals(b);
		else return false;
	}
	
	static private Document getDocument(XMLNode n, BidirectionalMap<Node,XMLNode> map) {
		if(n instanceof XMLDocument) {
			return ((XMLDocument) n).getDocument();
		} else
			return getDocument((XMLNode)n.eContainer(),map);
	}
	
	static private void backwardElement(XMLElement en,Element n, BidirectionalMap<Node,XMLNode> map) {
		backwardNode(en,n,map);
		
		{
			EList<XMLAttr> attrList = en.getAttributes();
			for(XMLAttr ea : attrList) {
				Attr a = (Attr)map.backward(ea);
				if(a==null) {
					a = (Attr)backwardInstance(ea,map);
				} else {
					if(!compareString(ea.getName(),a.getName())
							|| !compareString(ea.getLocalName(),a.getLocalName())
							|| !compareString(ea.getPrefix(),a.getPrefix())
							|| !compareString(ea.getNamespaceURI(),a.getNamespaceURI())) {
						map.removeForward(a);
						Attr na = (Attr)backwardInstance(ea,map);
						n.removeAttributeNode(a);
						a = na;
					}
				}
				n.getAttributes().setNamedItem(a);
				a.setNodeValue(ea.getValue());
			}
			
			NamedNodeMap nodeMap = n.getAttributes();
			if(nodeMap!=null) {
				ArrayList<Node> del = new ArrayList<Node>();
				int size = nodeMap.getLength();
				
				for(int i=0;i<size;i++) {
					Node an = nodeMap.item(i);
					XMLNode ean = map.forward(an);
					
					if(ean==null || ean.eContainer()==null) {
						map.removeForward(an);
						del.add(an);
					} else if(ean.eContainer()!=en){
						del.add(an);
					}
				}
				
				for(Node an : del) {
					nodeMap.removeNamedItem(an.getNodeName());
				}
			}
		}
	}
	
	
	private static Node backwardInstance(XMLNode ea,
			BidirectionalMap<Node, XMLNode> map) {
		Document d = getDocument(ea,map);
		Node n = null;
		
		if(ea instanceof XMLAttr) {
			if(((XMLAttr)ea).getLocalName()!=null && ((XMLAttr)ea).getPrefix()!=null && ea.getNamespaceURI()!=null)
				n = d.createAttributeNS(ea.getNamespaceURI(), ea.getNodeName());
			else
				n = d.createAttribute(ea.getNodeName());
			
		} else if(ea instanceof XMLElement) {
			if(((XMLElement)ea).getLocalName()!=null && ((XMLElement)ea).getPrefix()!=null && ea.getNamespaceURI()!=null)
				n = d.createElementNS(ea.getNamespaceURI(), ea.getNodeName());
			else
				n = d.createElement(ea.getNodeName());
		} else if(ea instanceof XMLProcessingInstruction) {
			n = d.createProcessingInstruction(
					((XMLProcessingInstruction) ea).getTarget(), 
					((XMLProcessingInstruction) ea).getData());
		} else if(ea instanceof XMLComment) {
			n = d.createComment(((XMLComment) ea).getData());
		} else if(ea instanceof XMLCDATASection) {
			n = d.createCDATASection(((XMLCDATASection) ea).getData());
		} else if(ea instanceof XMLText) {
			n = d.createTextNode(((XMLText) ea).getData());
		} else {
			throw new UnsupportedOperationException(ea.eClass().getName());
		}
		map.put(n,ea);
		return n;
	}
	
	static private void backwardProcessingInstruction(XMLProcessingInstruction ep, 
			ProcessingInstruction p,BidirectionalMap<Node,XMLNode> map) {
		backwardNode(ep,p,map);
		p.setData(ep.getData());
	}
	
	static private void backwardComment(XMLComment ec,Comment c,BidirectionalMap<Node,XMLNode> map) {
		backwardNode(ec,c,map);
		c.setData(ec.getData());
	}
	
	static private void backwardCDATASection(XMLCDATASection ec,CDATASection c,BidirectionalMap<Node,XMLNode> map) {
		backwardNode(ec,c,map);
		c.setData(ec.getData());
	}
	
	static private void backwardText(XMLText ec,Text c,BidirectionalMap<Node,XMLNode> map) {
		backwardNode(ec,c,map);
		c.setData(ec.getData());
	}

	static private void backwardNode(XMLNode en, Node n, BidirectionalMap<Node,XMLNode> map) {
		
		//n.setBaseURI(en.getBaseURI());//readOnly
				
		//en.setNamespaceURI(n.getNamespaceURI());
		//en.setNodeName(n.getNodeName());
		
		//backwardNodeType(n,en);//readOnly
		
		
		EList<XMLNode> childList = en.getChildNodes();
		{
			for(XMLNode ecn : childList) {
				Node cn = map.backward(ecn);
				if(cn==null) {
					cn = backwardInstance(ecn,map);
				} else {
					boolean replace = false;
					
					if(ecn instanceof XMLElement) {
						replace = !compareString(ecn.getNodeName(),cn.getNodeName())
								|| !compareString(ecn.getNamespaceURI(),cn.getNamespaceURI());
					} else {
						replace = !compareString(ecn.getNodeName(),cn.getNodeName());
					}
					
					if(replace) {
						map.removeForward(cn);
						Node ncn = backwardInstance(ecn,map);
						try {
							while(cn.getAttributes().getLength()!=0) {
								ncn.getAttributes().setNamedItem(cn.getAttributes().item(0));								
							}
						} catch (Exception e1) {}
						try{
							ncn.appendChild(cn.getFirstChild());
						} catch(Exception e1) {}
						
						n.replaceChild(ncn, cn);
						cn = ncn;
					}
				}
				
				backward(ecn,cn,map);
			}
			
			//ArrayList<Node> del = new ArrayList<Node>();
			Node cur = n.getFirstChild();
			
			while(cur!=null) {
				Node prev = cur;
				cur = cur.getNextSibling();
				
				XMLNode eprev = map.forward(prev);
				
				if(eprev==null || eprev.eContainer()==null) {
					map.removeForward(prev);
					n.removeChild(prev);
				} else  if(eprev.eContainer()!=en) {
					n.removeChild(prev);
				}
			}
		}
	}

	private static void backward(XMLNode ecn, Node cn,
			BidirectionalMap<Node, XMLNode> map) {
		if(ecn instanceof XMLAttr) {
			throw new UnsupportedOperationException();
		} else if(ecn instanceof XMLElement) {
			backwardElement((XMLElement)ecn,(Element)cn,map);
		} else if(ecn instanceof XMLProcessingInstruction) {
			backwardProcessingInstruction((XMLProcessingInstruction)ecn,(ProcessingInstruction)cn,map);
		} else if(ecn instanceof XMLComment) {
			backwardComment((XMLComment)ecn,(Comment)cn,map);
		} else if(ecn instanceof XMLCDATASection) {
			backwardCDATASection((XMLCDATASection)ecn,(CDATASection)cn,map);
		} else if(ecn instanceof XMLText) {
			backwardText((XMLText)ecn,(Text)cn,map);
		} else {
			throw new UnsupportedOperationException();
		}
	}

	private static void forward(Node cn, XMLNode ecn,
			BidirectionalMap<Node, XMLNode> map) {
		if(cn instanceof Attr) {
			forwardAttr((Attr)cn,(XMLAttr)ecn,map);
		} else if(cn instanceof Element) {
			forwardElement((Element)cn,(XMLElement)ecn,map);
		} else if(cn instanceof ProcessingInstruction) {
			forwardProcessingInstruction((ProcessingInstruction)cn,(XMLProcessingInstruction)ecn,map);
		} else if(cn instanceof Comment) {
			forwardComment((Comment)cn,(XMLComment)ecn,map);
		} else if(cn instanceof CDATASection) {
			forwardCDATASection((CDATASection)cn,(XMLCDATASection)ecn,map);
		} else if(cn instanceof Text) {
			forwardText((Text)cn,(XMLText)ecn,map);
		} else {
			throw new UnsupportedOperationException(cn.getClass().getName());
		}
	}

	private static void forwardCDATASection(CDATASection cn, XMLCDATASection ecn,
			BidirectionalMap<Node, XMLNode> map) {
		forwardNode(cn,ecn,map);
		ecn.setData(cn.getData());
	}

	private static void forwardText(Text cn,XMLText ecn,BidirectionalMap<Node, XMLNode> map) {
		forwardNode(cn,ecn,map);
		ecn.setData(cn.getData());
	}

	private static void forwardComment(Comment cn, XMLComment ecn,
			BidirectionalMap<Node, XMLNode> map) {
		forwardNode(cn,ecn,map);
		ecn.setData(cn.getData());
	}

	private static void forwardProcessingInstruction(ProcessingInstruction cn,
			XMLProcessingInstruction ecn, BidirectionalMap<Node, XMLNode> map) {
		
		forwardNode(cn,ecn,map);
		ecn.setData(cn.getData());
		ecn.setData(cn.getData());
	}

	private static void forwardElement(Element n, XMLElement en,
			BidirectionalMap<Node, XMLNode> map) {
		forwardNode(n,en,map);
		//ecn.setTagName(value);
		//element
		NamedNodeMap nodeMap = n.getAttributes();
		if(nodeMap!=null) {
			int size = nodeMap.getLength();
			for(int i=0;i<size;i++) {
				Attr a = (Attr)nodeMap.item(i);
				XMLAttr ea = (XMLAttr)map.forward(a);
				if(ea==null) {
					ea = (XMLAttr) forwardInstance(a,map);
				}
				
				if(en.getAttributes().contains(ea)==false)
					en.getAttributes().add(ea);
				
				forwardAttr(a,ea,map);
			}
			
			ArrayList<XMLAttr> del = new ArrayList<XMLAttr>(); 
			
			for(XMLAttr ea : en.getAttributes()) {
				Attr a = (Attr)map.backward(ea);
				if(a==null || a.getOwnerElement()==null) {
					del.add(ea);
					map.removeBackward(ea);
				} else if(a.getOwnerElement()!=n) {
					del.add(ea);
				}
			}
			
			en.getAttributes().removeAll(del);
		}
	}

	private static XMLNode forwardInstance(Node cn, 
			BidirectionalMap<Node, XMLNode> map) {
		XMLNode ecn = null;
		if(cn instanceof Attr) {
			ecn = EXMLFactory.eINSTANCE.createXMLAttr();
		} else if(cn instanceof Element) {
			ecn = EXMLFactory.eINSTANCE.createXMLElement();
		} else if(cn instanceof ProcessingInstruction) {
			ecn = EXMLFactory.eINSTANCE.createXMLProcessingInstruction();
		} else if(cn instanceof Comment) {
			ecn = EXMLFactory.eINSTANCE.createXMLComment();
		} else if(cn instanceof CDATASection) {
			ecn = EXMLFactory.eINSTANCE.createXMLCDATASection();
		} else if(cn instanceof Text) {
			ecn = EXMLFactory.eINSTANCE.createXMLText();
		} else {
			throw new UnsupportedOperationException(cn.getClass().getName());
		}
		map.put(cn, ecn);
		return ecn;
	}

	private static void forwardAttr(Attr a, XMLAttr ea,
			BidirectionalMap<Node, XMLNode> map) {
		forwardNode(a,ea,map);
		ea.setValue(a.getValue());
	}

	static private void forwardDoctype(DocumentType dt,XMLDocumentType edt,BidirectionalMap<Node,XMLNode> map) {
	//		edt.setBaseURI(dt.getBaseURI());
	//		edt.setNamespaceURI(dt.getNamespaceURI());
	//		edt.setNodeName(dt.getName());
	//		//edt.setNodeType(dt.getNodeType());
	//		
		}

	private static void forwardNodeType(Node n, XMLNode en) {
		switch(n.getNodeType()) {
		case Node.ATTRIBUTE_NODE:
			en.setNodeType(NodeType.ATTRIBUTE_NODE);
			break;
		case Node.CDATA_SECTION_NODE:
			en.setNodeType(NodeType.CDATA_SECTION_NODE);
			break;
		case Node.COMMENT_NODE:
			en.setNodeType(NodeType.COMMENT_NODE);
			break;
		case Node.DOCUMENT_FRAGMENT_NODE:
			en.setNodeType(NodeType.DOCUMENT_FRAGMENT_NODE);
			break;
		case Node.DOCUMENT_NODE:
			en.setNodeType(NodeType.DOCUMENT_NODE);
			break;
		case Node.DOCUMENT_TYPE_NODE:
			en.setNodeType(NodeType.DOCUMENT_TYPE_NODE);
			break;
		case Node.ELEMENT_NODE:
			en.setNodeType(NodeType.ELEMENT_NODE);
			break;
		case Node.ENTITY_NODE:
			en.setNodeType(NodeType.ENTITY_NODE);
			break;
		case Node.ENTITY_REFERENCE_NODE:
			en.setNodeType(NodeType.ENTITY_REFERENCE_NODE);
			break;
		case Node.NOTATION_NODE:
			en.setNodeType(NodeType.NOTATION_NODE);
			break;
		case Node.PROCESSING_INSTRUCTION_NODE:
			en.setNodeType(NodeType.PROCESSING_INSTRUCTION_NODE);
			break;
		case Node.TEXT_NODE:
			en.setNodeType(NodeType.TEXT_NODE);
			break;
		}
	}

}
