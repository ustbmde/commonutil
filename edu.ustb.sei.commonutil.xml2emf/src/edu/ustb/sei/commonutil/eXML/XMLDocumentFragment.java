/**
 */
package edu.ustb.sei.commonutil.eXML;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XML Document Fragment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLDocumentFragment()
 * @model
 * @generated
 */
public interface XMLDocumentFragment extends XMLNode {
} // XMLDocumentFragment
