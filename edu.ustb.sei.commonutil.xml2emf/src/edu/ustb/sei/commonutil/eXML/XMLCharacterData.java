/**
 */
package edu.ustb.sei.commonutil.eXML;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XML Character Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLCharacterData#getData <em>Data</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLCharacterData#getNodeValue <em>Node Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLCharacterData()
 * @model abstract="true"
 * @generated
 */
public interface XMLCharacterData extends XMLNode {
	/**
	 * Returns the value of the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data</em>' attribute.
	 * @see #setData(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLCharacterData_Data()
	 * @model
	 * @generated
	 */
	String getData();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLCharacterData#getData <em>Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data</em>' attribute.
	 * @see #getData()
	 * @generated
	 */
	void setData(String value);

	/**
	 * Returns the value of the '<em><b>Node Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Value</em>' attribute.
	 * @see #setNodeValue(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLCharacterData_NodeValue()
	 * @model
	 * @generated
	 */
	String getNodeValue();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLCharacterData#getNodeValue <em>Node Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Value</em>' attribute.
	 * @see #getNodeValue()
	 * @generated
	 */
	void setNodeValue(String value);

} // XMLCharacterData
