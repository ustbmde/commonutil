/**
 */
package edu.ustb.sei.commonutil.eXML;

import org.w3c.dom.Document;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XML Document</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getDoctype <em>Doctype</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getDocumentURI <em>Document URI</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getInputEncoding <em>Input Encoding</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLDocument#isStrictErrorChecking <em>Strict Error Checking</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getXmlVersion <em>Xml Version</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getXmlEncoding <em>Xml Encoding</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLDocument#isXmlStandalone <em>Xml Standalone</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getDocument <em>Document</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLDocument()
 * @model
 * @generated
 */
public interface XMLDocument extends XMLNode {

	/**
	 * Returns the value of the '<em><b>Doctype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Doctype</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Doctype</em>' containment reference.
	 * @see #setDoctype(XMLDocumentType)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLDocument_Doctype()
	 * @model containment="true"
	 * @generated
	 */
	XMLDocumentType getDoctype();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getDoctype <em>Doctype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Doctype</em>' containment reference.
	 * @see #getDoctype()
	 * @generated
	 */
	void setDoctype(XMLDocumentType value);

	/**
	 * Returns the value of the '<em><b>Document URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Document URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Document URI</em>' attribute.
	 * @see #setDocumentURI(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLDocument_DocumentURI()
	 * @model
	 * @generated
	 */
	String getDocumentURI();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getDocumentURI <em>Document URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Document URI</em>' attribute.
	 * @see #getDocumentURI()
	 * @generated
	 */
	void setDocumentURI(String value);

	/**
	 * Returns the value of the '<em><b>Input Encoding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Encoding</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Encoding</em>' attribute.
	 * @see #setInputEncoding(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLDocument_InputEncoding()
	 * @model
	 * @generated
	 */
	String getInputEncoding();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getInputEncoding <em>Input Encoding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Encoding</em>' attribute.
	 * @see #getInputEncoding()
	 * @generated
	 */
	void setInputEncoding(String value);

	/**
	 * Returns the value of the '<em><b>Strict Error Checking</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strict Error Checking</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strict Error Checking</em>' attribute.
	 * @see #setStrictErrorChecking(boolean)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLDocument_StrictErrorChecking()
	 * @model default="false"
	 * @generated
	 */
	boolean isStrictErrorChecking();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#isStrictErrorChecking <em>Strict Error Checking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strict Error Checking</em>' attribute.
	 * @see #isStrictErrorChecking()
	 * @generated
	 */
	void setStrictErrorChecking(boolean value);

	/**
	 * Returns the value of the '<em><b>Xml Version</b></em>' attribute.
	 * The default value is <code>"1.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xml Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xml Version</em>' attribute.
	 * @see #setXmlVersion(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLDocument_XmlVersion()
	 * @model default="1.0"
	 * @generated
	 */
	String getXmlVersion();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getXmlVersion <em>Xml Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Xml Version</em>' attribute.
	 * @see #getXmlVersion()
	 * @generated
	 */
	void setXmlVersion(String value);

	/**
	 * Returns the value of the '<em><b>Xml Encoding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xml Encoding</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xml Encoding</em>' attribute.
	 * @see #setXmlEncoding(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLDocument_XmlEncoding()
	 * @model
	 * @generated
	 */
	String getXmlEncoding();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getXmlEncoding <em>Xml Encoding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Xml Encoding</em>' attribute.
	 * @see #getXmlEncoding()
	 * @generated
	 */
	void setXmlEncoding(String value);

	/**
	 * Returns the value of the '<em><b>Xml Standalone</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xml Standalone</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xml Standalone</em>' attribute.
	 * @see #setXmlStandalone(boolean)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLDocument_XmlStandalone()
	 * @model default="true"
	 * @generated
	 */
	boolean isXmlStandalone();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#isXmlStandalone <em>Xml Standalone</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Xml Standalone</em>' attribute.
	 * @see #isXmlStandalone()
	 * @generated
	 */
	void setXmlStandalone(boolean value);

	/**
	 * Returns the value of the '<em><b>Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Document</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Document</em>' attribute.
	 * @see #setDocument(Document)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLDocument_Document()
	 * @model dataType="edu.ustb.sei.commonutil.eXML.JavaXMLDocument"
	 * @generated
	 */
	Document getDocument();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getDocument <em>Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Document</em>' attribute.
	 * @see #getDocument()
	 * @generated
	 */
	void setDocument(Document value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void forward();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void backward();
} // XMLDocument
