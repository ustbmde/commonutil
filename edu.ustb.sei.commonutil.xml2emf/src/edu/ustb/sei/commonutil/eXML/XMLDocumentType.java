/**
 */
package edu.ustb.sei.commonutil.eXML;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XML Document Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLDocumentType()
 * @model
 * @generated
 */
public interface XMLDocumentType extends XMLNode {
} // XMLDocumentType
