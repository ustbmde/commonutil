/**
 */
package edu.ustb.sei.commonutil.eXML;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XML Entity Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLEntityReference()
 * @model
 * @generated
 */
public interface XMLEntityReference extends XMLNode {
} // XMLEntityReference
