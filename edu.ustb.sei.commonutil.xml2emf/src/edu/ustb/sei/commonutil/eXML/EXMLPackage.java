/**
 */
package edu.ustb.sei.commonutil.eXML;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.commonutil.eXML.EXMLFactory
 * @model kind="package"
 * @generated
 */
public interface EXMLPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "eXML";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/commonutil/xml2emf";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "eXML";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EXMLPackage eINSTANCE = edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLNodeImpl <em>XML Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLNodeImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLNode()
	 * @generated
	 */
	int XML_NODE = 0;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NODE__BASE_URI = 0;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NODE__CHILD_NODES = 1;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NODE__ATTRIBUTES = 2;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NODE__NAMESPACE_URI = 3;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NODE__NODE_NAME = 4;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NODE__NODE_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NODE__PARENT_NODE = 6;

	/**
	 * The number of structural features of the '<em>XML Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NODE_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>XML Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLAttrImpl <em>XML Attr</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLAttrImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLAttr()
	 * @generated
	 */
	int XML_ATTR = 1;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR__BASE_URI = XML_NODE__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR__CHILD_NODES = XML_NODE__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR__ATTRIBUTES = XML_NODE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR__NAMESPACE_URI = XML_NODE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR__NODE_NAME = XML_NODE__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR__NODE_TYPE = XML_NODE__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR__PARENT_NODE = XML_NODE__PARENT_NODE;

	/**
	 * The feature id for the '<em><b>Node Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR__NODE_VALUE = XML_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR__NAME = XML_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR__VALUE = XML_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Local Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR__LOCAL_NAME = XML_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR__PREFIX = XML_NODE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>XML Attr</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR_FEATURE_COUNT = XML_NODE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>XML Attr</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTR_OPERATION_COUNT = XML_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLCDATASectionImpl <em>XMLCDATA Section</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLCDATASectionImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLCDATASection()
	 * @generated
	 */
	int XMLCDATA_SECTION = 2;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLCharacterDataImpl <em>XML Character Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLCharacterDataImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLCharacterData()
	 * @generated
	 */
	int XML_CHARACTER_DATA = 3;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLCommentImpl <em>XML Comment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLCommentImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLComment()
	 * @generated
	 */
	int XML_COMMENT = 4;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentImpl <em>XML Document</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLDocumentImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLDocument()
	 * @generated
	 */
	int XML_DOCUMENT = 5;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentFragmentImpl <em>XML Document Fragment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLDocumentFragmentImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLDocumentFragment()
	 * @generated
	 */
	int XML_DOCUMENT_FRAGMENT = 6;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentTypeImpl <em>XML Document Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLDocumentTypeImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLDocumentType()
	 * @generated
	 */
	int XML_DOCUMENT_TYPE = 7;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLElementImpl <em>XML Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLElementImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLElement()
	 * @generated
	 */
	int XML_ELEMENT = 8;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLEntityImpl <em>XML Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLEntityImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLEntity()
	 * @generated
	 */
	int XML_ENTITY = 9;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLEntityReferenceImpl <em>XML Entity Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLEntityReferenceImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLEntityReference()
	 * @generated
	 */
	int XML_ENTITY_REFERENCE = 10;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLNotationImpl <em>XML Notation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLNotationImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLNotation()
	 * @generated
	 */
	int XML_NOTATION = 11;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLProcessingInstructionImpl <em>XML Processing Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLProcessingInstructionImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLProcessingInstruction()
	 * @generated
	 */
	int XML_PROCESSING_INSTRUCTION = 12;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLTextImpl <em>XML Text</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.impl.XMLTextImpl
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLText()
	 * @generated
	 */
	int XML_TEXT = 13;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_CHARACTER_DATA__BASE_URI = XML_NODE__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_CHARACTER_DATA__CHILD_NODES = XML_NODE__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_CHARACTER_DATA__ATTRIBUTES = XML_NODE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_CHARACTER_DATA__NAMESPACE_URI = XML_NODE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_CHARACTER_DATA__NODE_NAME = XML_NODE__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_CHARACTER_DATA__NODE_TYPE = XML_NODE__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_CHARACTER_DATA__PARENT_NODE = XML_NODE__PARENT_NODE;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_CHARACTER_DATA__DATA = XML_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Node Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_CHARACTER_DATA__NODE_VALUE = XML_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>XML Character Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_CHARACTER_DATA_FEATURE_COUNT = XML_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>XML Character Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_CHARACTER_DATA_OPERATION_COUNT = XML_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_TEXT__BASE_URI = XML_CHARACTER_DATA__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_TEXT__CHILD_NODES = XML_CHARACTER_DATA__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_TEXT__ATTRIBUTES = XML_CHARACTER_DATA__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_TEXT__NAMESPACE_URI = XML_CHARACTER_DATA__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_TEXT__NODE_NAME = XML_CHARACTER_DATA__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_TEXT__NODE_TYPE = XML_CHARACTER_DATA__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_TEXT__PARENT_NODE = XML_CHARACTER_DATA__PARENT_NODE;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_TEXT__DATA = XML_CHARACTER_DATA__DATA;

	/**
	 * The feature id for the '<em><b>Node Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_TEXT__NODE_VALUE = XML_CHARACTER_DATA__NODE_VALUE;

	/**
	 * The number of structural features of the '<em>XML Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_TEXT_FEATURE_COUNT = XML_CHARACTER_DATA_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>XML Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_TEXT_OPERATION_COUNT = XML_CHARACTER_DATA_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMLCDATA_SECTION__BASE_URI = XML_TEXT__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMLCDATA_SECTION__CHILD_NODES = XML_TEXT__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMLCDATA_SECTION__ATTRIBUTES = XML_TEXT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMLCDATA_SECTION__NAMESPACE_URI = XML_TEXT__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMLCDATA_SECTION__NODE_NAME = XML_TEXT__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMLCDATA_SECTION__NODE_TYPE = XML_TEXT__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMLCDATA_SECTION__PARENT_NODE = XML_TEXT__PARENT_NODE;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMLCDATA_SECTION__DATA = XML_TEXT__DATA;

	/**
	 * The feature id for the '<em><b>Node Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMLCDATA_SECTION__NODE_VALUE = XML_TEXT__NODE_VALUE;

	/**
	 * The number of structural features of the '<em>XMLCDATA Section</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMLCDATA_SECTION_FEATURE_COUNT = XML_TEXT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>XMLCDATA Section</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XMLCDATA_SECTION_OPERATION_COUNT = XML_TEXT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_COMMENT__BASE_URI = XML_CHARACTER_DATA__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_COMMENT__CHILD_NODES = XML_CHARACTER_DATA__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_COMMENT__ATTRIBUTES = XML_CHARACTER_DATA__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_COMMENT__NAMESPACE_URI = XML_CHARACTER_DATA__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_COMMENT__NODE_NAME = XML_CHARACTER_DATA__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_COMMENT__NODE_TYPE = XML_CHARACTER_DATA__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_COMMENT__PARENT_NODE = XML_CHARACTER_DATA__PARENT_NODE;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_COMMENT__DATA = XML_CHARACTER_DATA__DATA;

	/**
	 * The feature id for the '<em><b>Node Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_COMMENT__NODE_VALUE = XML_CHARACTER_DATA__NODE_VALUE;

	/**
	 * The number of structural features of the '<em>XML Comment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_COMMENT_FEATURE_COUNT = XML_CHARACTER_DATA_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>XML Comment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_COMMENT_OPERATION_COUNT = XML_CHARACTER_DATA_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__BASE_URI = XML_NODE__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__CHILD_NODES = XML_NODE__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__ATTRIBUTES = XML_NODE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__NAMESPACE_URI = XML_NODE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__NODE_NAME = XML_NODE__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__NODE_TYPE = XML_NODE__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__PARENT_NODE = XML_NODE__PARENT_NODE;

	/**
	 * The feature id for the '<em><b>Doctype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__DOCTYPE = XML_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Document URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__DOCUMENT_URI = XML_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Input Encoding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__INPUT_ENCODING = XML_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Strict Error Checking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__STRICT_ERROR_CHECKING = XML_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Xml Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__XML_VERSION = XML_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Xml Encoding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__XML_ENCODING = XML_NODE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Xml Standalone</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__XML_STANDALONE = XML_NODE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Document</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT__DOCUMENT = XML_NODE_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>XML Document</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_FEATURE_COUNT = XML_NODE_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Forward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT___FORWARD = XML_NODE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Backward</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT___BACKWARD = XML_NODE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>XML Document</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_OPERATION_COUNT = XML_NODE_OPERATION_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_FRAGMENT__BASE_URI = XML_NODE__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_FRAGMENT__CHILD_NODES = XML_NODE__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_FRAGMENT__ATTRIBUTES = XML_NODE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_FRAGMENT__NAMESPACE_URI = XML_NODE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_FRAGMENT__NODE_NAME = XML_NODE__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_FRAGMENT__NODE_TYPE = XML_NODE__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_FRAGMENT__PARENT_NODE = XML_NODE__PARENT_NODE;

	/**
	 * The number of structural features of the '<em>XML Document Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_FRAGMENT_FEATURE_COUNT = XML_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>XML Document Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_FRAGMENT_OPERATION_COUNT = XML_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_TYPE__BASE_URI = XML_NODE__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_TYPE__CHILD_NODES = XML_NODE__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_TYPE__ATTRIBUTES = XML_NODE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_TYPE__NAMESPACE_URI = XML_NODE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_TYPE__NODE_NAME = XML_NODE__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_TYPE__NODE_TYPE = XML_NODE__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_TYPE__PARENT_NODE = XML_NODE__PARENT_NODE;

	/**
	 * The number of structural features of the '<em>XML Document Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_TYPE_FEATURE_COUNT = XML_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>XML Document Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_DOCUMENT_TYPE_OPERATION_COUNT = XML_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT__BASE_URI = XML_NODE__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT__CHILD_NODES = XML_NODE__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT__ATTRIBUTES = XML_NODE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT__NAMESPACE_URI = XML_NODE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT__NODE_NAME = XML_NODE__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT__NODE_TYPE = XML_NODE__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT__PARENT_NODE = XML_NODE__PARENT_NODE;

	/**
	 * The feature id for the '<em><b>Tag Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT__TAG_NAME = XML_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Local Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT__LOCAL_NAME = XML_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT__PREFIX = XML_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>XML Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT_FEATURE_COUNT = XML_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>XML Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ELEMENT_OPERATION_COUNT = XML_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY__BASE_URI = XML_NODE__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY__CHILD_NODES = XML_NODE__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY__ATTRIBUTES = XML_NODE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY__NAMESPACE_URI = XML_NODE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY__NODE_NAME = XML_NODE__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY__NODE_TYPE = XML_NODE__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY__PARENT_NODE = XML_NODE__PARENT_NODE;

	/**
	 * The number of structural features of the '<em>XML Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY_FEATURE_COUNT = XML_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>XML Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY_OPERATION_COUNT = XML_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY_REFERENCE__BASE_URI = XML_NODE__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY_REFERENCE__CHILD_NODES = XML_NODE__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY_REFERENCE__ATTRIBUTES = XML_NODE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY_REFERENCE__NAMESPACE_URI = XML_NODE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY_REFERENCE__NODE_NAME = XML_NODE__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY_REFERENCE__NODE_TYPE = XML_NODE__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY_REFERENCE__PARENT_NODE = XML_NODE__PARENT_NODE;

	/**
	 * The number of structural features of the '<em>XML Entity Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY_REFERENCE_FEATURE_COUNT = XML_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>XML Entity Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ENTITY_REFERENCE_OPERATION_COUNT = XML_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NOTATION__BASE_URI = XML_NODE__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NOTATION__CHILD_NODES = XML_NODE__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NOTATION__ATTRIBUTES = XML_NODE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NOTATION__NAMESPACE_URI = XML_NODE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NOTATION__NODE_NAME = XML_NODE__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NOTATION__NODE_TYPE = XML_NODE__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NOTATION__PARENT_NODE = XML_NODE__PARENT_NODE;

	/**
	 * The number of structural features of the '<em>XML Notation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NOTATION_FEATURE_COUNT = XML_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>XML Notation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_NOTATION_OPERATION_COUNT = XML_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Base URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_PROCESSING_INSTRUCTION__BASE_URI = XML_NODE__BASE_URI;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_PROCESSING_INSTRUCTION__CHILD_NODES = XML_NODE__CHILD_NODES;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_PROCESSING_INSTRUCTION__ATTRIBUTES = XML_NODE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_PROCESSING_INSTRUCTION__NAMESPACE_URI = XML_NODE__NAMESPACE_URI;

	/**
	 * The feature id for the '<em><b>Node Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_PROCESSING_INSTRUCTION__NODE_NAME = XML_NODE__NODE_NAME;

	/**
	 * The feature id for the '<em><b>Node Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_PROCESSING_INSTRUCTION__NODE_TYPE = XML_NODE__NODE_TYPE;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_PROCESSING_INSTRUCTION__PARENT_NODE = XML_NODE__PARENT_NODE;

	/**
	 * The feature id for the '<em><b>Node Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_PROCESSING_INSTRUCTION__NODE_VALUE = XML_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_PROCESSING_INSTRUCTION__DATA = XML_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_PROCESSING_INSTRUCTION__TARGET = XML_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>XML Processing Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_PROCESSING_INSTRUCTION_FEATURE_COUNT = XML_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>XML Processing Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_PROCESSING_INSTRUCTION_OPERATION_COUNT = XML_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.commonutil.eXML.NodeType <em>Node Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.commonutil.eXML.NodeType
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getNodeType()
	 * @generated
	 */
	int NODE_TYPE = 14;


	/**
	 * The meta object id for the '<em>Java XML Node</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3c.dom.Node
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getJavaXMLNode()
	 * @generated
	 */
	int JAVA_XML_NODE = 15;


	/**
	 * The meta object id for the '<em>Java XML Document</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.w3c.dom.Document
	 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getJavaXMLDocument()
	 * @generated
	 */
	int JAVA_XML_DOCUMENT = 16;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLNode <em>XML Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Node</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLNode
	 * @generated
	 */
	EClass getXMLNode();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getBaseURI <em>Base URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base URI</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLNode#getBaseURI()
	 * @see #getXMLNode()
	 * @generated
	 */
	EAttribute getXMLNode_BaseURI();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getChildNodes <em>Child Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Child Nodes</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLNode#getChildNodes()
	 * @see #getXMLNode()
	 * @generated
	 */
	EReference getXMLNode_ChildNodes();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLNode#getAttributes()
	 * @see #getXMLNode()
	 * @generated
	 */
	EReference getXMLNode_Attributes();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getNamespaceURI <em>Namespace URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Namespace URI</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLNode#getNamespaceURI()
	 * @see #getXMLNode()
	 * @generated
	 */
	EAttribute getXMLNode_NamespaceURI();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getNodeName <em>Node Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Node Name</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLNode#getNodeName()
	 * @see #getXMLNode()
	 * @generated
	 */
	EAttribute getXMLNode_NodeName();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getNodeType <em>Node Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Node Type</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLNode#getNodeType()
	 * @see #getXMLNode()
	 * @generated
	 */
	EAttribute getXMLNode_NodeType();

	/**
	 * Returns the meta object for the container reference '{@link edu.ustb.sei.commonutil.eXML.XMLNode#getParentNode <em>Parent Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Node</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLNode#getParentNode()
	 * @see #getXMLNode()
	 * @generated
	 */
	EReference getXMLNode_ParentNode();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLAttr <em>XML Attr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Attr</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLAttr
	 * @generated
	 */
	EClass getXMLAttr();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getNodeValue <em>Node Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Node Value</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLAttr#getNodeValue()
	 * @see #getXMLAttr()
	 * @generated
	 */
	EAttribute getXMLAttr_NodeValue();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLAttr#getName()
	 * @see #getXMLAttr()
	 * @generated
	 */
	EAttribute getXMLAttr_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLAttr#getValue()
	 * @see #getXMLAttr()
	 * @generated
	 */
	EAttribute getXMLAttr_Value();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getLocalName <em>Local Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Local Name</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLAttr#getLocalName()
	 * @see #getXMLAttr()
	 * @generated
	 */
	EAttribute getXMLAttr_LocalName();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getPrefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Prefix</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLAttr#getPrefix()
	 * @see #getXMLAttr()
	 * @generated
	 */
	EAttribute getXMLAttr_Prefix();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLCDATASection <em>XMLCDATA Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XMLCDATA Section</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLCDATASection
	 * @generated
	 */
	EClass getXMLCDATASection();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLCharacterData <em>XML Character Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Character Data</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLCharacterData
	 * @generated
	 */
	EClass getXMLCharacterData();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLCharacterData#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLCharacterData#getData()
	 * @see #getXMLCharacterData()
	 * @generated
	 */
	EAttribute getXMLCharacterData_Data();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLCharacterData#getNodeValue <em>Node Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Node Value</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLCharacterData#getNodeValue()
	 * @see #getXMLCharacterData()
	 * @generated
	 */
	EAttribute getXMLCharacterData_NodeValue();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLComment <em>XML Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Comment</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLComment
	 * @generated
	 */
	EClass getXMLComment();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLDocument <em>XML Document</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Document</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocument
	 * @generated
	 */
	EClass getXMLDocument();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getDoctype <em>Doctype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Doctype</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocument#getDoctype()
	 * @see #getXMLDocument()
	 * @generated
	 */
	EReference getXMLDocument_Doctype();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getDocumentURI <em>Document URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Document URI</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocument#getDocumentURI()
	 * @see #getXMLDocument()
	 * @generated
	 */
	EAttribute getXMLDocument_DocumentURI();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getInputEncoding <em>Input Encoding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Encoding</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocument#getInputEncoding()
	 * @see #getXMLDocument()
	 * @generated
	 */
	EAttribute getXMLDocument_InputEncoding();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#isStrictErrorChecking <em>Strict Error Checking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Strict Error Checking</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocument#isStrictErrorChecking()
	 * @see #getXMLDocument()
	 * @generated
	 */
	EAttribute getXMLDocument_StrictErrorChecking();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getXmlVersion <em>Xml Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Xml Version</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocument#getXmlVersion()
	 * @see #getXMLDocument()
	 * @generated
	 */
	EAttribute getXMLDocument_XmlVersion();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getXmlEncoding <em>Xml Encoding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Xml Encoding</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocument#getXmlEncoding()
	 * @see #getXMLDocument()
	 * @generated
	 */
	EAttribute getXMLDocument_XmlEncoding();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#isXmlStandalone <em>Xml Standalone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Xml Standalone</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocument#isXmlStandalone()
	 * @see #getXMLDocument()
	 * @generated
	 */
	EAttribute getXMLDocument_XmlStandalone();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#getDocument <em>Document</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Document</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocument#getDocument()
	 * @see #getXMLDocument()
	 * @generated
	 */
	EAttribute getXMLDocument_Document();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#forward() <em>Forward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Forward</em>' operation.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocument#forward()
	 * @generated
	 */
	EOperation getXMLDocument__Forward();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.commonutil.eXML.XMLDocument#backward() <em>Backward</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Backward</em>' operation.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocument#backward()
	 * @generated
	 */
	EOperation getXMLDocument__Backward();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLDocumentFragment <em>XML Document Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Document Fragment</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocumentFragment
	 * @generated
	 */
	EClass getXMLDocumentFragment();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLDocumentType <em>XML Document Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Document Type</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLDocumentType
	 * @generated
	 */
	EClass getXMLDocumentType();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLElement <em>XML Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Element</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLElement
	 * @generated
	 */
	EClass getXMLElement();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLElement#getTagName <em>Tag Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tag Name</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLElement#getTagName()
	 * @see #getXMLElement()
	 * @generated
	 */
	EAttribute getXMLElement_TagName();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLElement#getLocalName <em>Local Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Local Name</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLElement#getLocalName()
	 * @see #getXMLElement()
	 * @generated
	 */
	EAttribute getXMLElement_LocalName();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLElement#getPrefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Prefix</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLElement#getPrefix()
	 * @see #getXMLElement()
	 * @generated
	 */
	EAttribute getXMLElement_Prefix();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLEntity <em>XML Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Entity</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLEntity
	 * @generated
	 */
	EClass getXMLEntity();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLEntityReference <em>XML Entity Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Entity Reference</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLEntityReference
	 * @generated
	 */
	EClass getXMLEntityReference();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLNotation <em>XML Notation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Notation</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLNotation
	 * @generated
	 */
	EClass getXMLNotation();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction <em>XML Processing Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Processing Instruction</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction
	 * @generated
	 */
	EClass getXMLProcessingInstruction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction#getNodeValue <em>Node Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Node Value</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction#getNodeValue()
	 * @see #getXMLProcessingInstruction()
	 * @generated
	 */
	EAttribute getXMLProcessingInstruction_NodeValue();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction#getData()
	 * @see #getXMLProcessingInstruction()
	 * @generated
	 */
	EAttribute getXMLProcessingInstruction_Data();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction#getTarget()
	 * @see #getXMLProcessingInstruction()
	 * @generated
	 */
	EAttribute getXMLProcessingInstruction_Target();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.commonutil.eXML.XMLText <em>XML Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Text</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.XMLText
	 * @generated
	 */
	EClass getXMLText();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.commonutil.eXML.NodeType <em>Node Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Node Type</em>'.
	 * @see edu.ustb.sei.commonutil.eXML.NodeType
	 * @generated
	 */
	EEnum getNodeType();

	/**
	 * Returns the meta object for data type '{@link org.w3c.dom.Node <em>Java XML Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Java XML Node</em>'.
	 * @see org.w3c.dom.Node
	 * @model instanceClass="org.w3c.dom.Node"
	 * @generated
	 */
	EDataType getJavaXMLNode();

	/**
	 * Returns the meta object for data type '{@link org.w3c.dom.Document <em>Java XML Document</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Java XML Document</em>'.
	 * @see org.w3c.dom.Document
	 * @model instanceClass="org.w3c.dom.Document"
	 * @generated
	 */
	EDataType getJavaXMLDocument();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EXMLFactory getEXMLFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLNodeImpl <em>XML Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLNodeImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLNode()
		 * @generated
		 */
		EClass XML_NODE = eINSTANCE.getXMLNode();

		/**
		 * The meta object literal for the '<em><b>Base URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_NODE__BASE_URI = eINSTANCE.getXMLNode_BaseURI();

		/**
		 * The meta object literal for the '<em><b>Child Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XML_NODE__CHILD_NODES = eINSTANCE.getXMLNode_ChildNodes();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XML_NODE__ATTRIBUTES = eINSTANCE.getXMLNode_Attributes();

		/**
		 * The meta object literal for the '<em><b>Namespace URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_NODE__NAMESPACE_URI = eINSTANCE.getXMLNode_NamespaceURI();

		/**
		 * The meta object literal for the '<em><b>Node Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_NODE__NODE_NAME = eINSTANCE.getXMLNode_NodeName();

		/**
		 * The meta object literal for the '<em><b>Node Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_NODE__NODE_TYPE = eINSTANCE.getXMLNode_NodeType();

		/**
		 * The meta object literal for the '<em><b>Parent Node</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XML_NODE__PARENT_NODE = eINSTANCE.getXMLNode_ParentNode();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLAttrImpl <em>XML Attr</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLAttrImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLAttr()
		 * @generated
		 */
		EClass XML_ATTR = eINSTANCE.getXMLAttr();

		/**
		 * The meta object literal for the '<em><b>Node Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_ATTR__NODE_VALUE = eINSTANCE.getXMLAttr_NodeValue();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_ATTR__NAME = eINSTANCE.getXMLAttr_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_ATTR__VALUE = eINSTANCE.getXMLAttr_Value();

		/**
		 * The meta object literal for the '<em><b>Local Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_ATTR__LOCAL_NAME = eINSTANCE.getXMLAttr_LocalName();

		/**
		 * The meta object literal for the '<em><b>Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_ATTR__PREFIX = eINSTANCE.getXMLAttr_Prefix();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLCDATASectionImpl <em>XMLCDATA Section</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLCDATASectionImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLCDATASection()
		 * @generated
		 */
		EClass XMLCDATA_SECTION = eINSTANCE.getXMLCDATASection();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLCharacterDataImpl <em>XML Character Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLCharacterDataImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLCharacterData()
		 * @generated
		 */
		EClass XML_CHARACTER_DATA = eINSTANCE.getXMLCharacterData();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_CHARACTER_DATA__DATA = eINSTANCE.getXMLCharacterData_Data();

		/**
		 * The meta object literal for the '<em><b>Node Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_CHARACTER_DATA__NODE_VALUE = eINSTANCE.getXMLCharacterData_NodeValue();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLCommentImpl <em>XML Comment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLCommentImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLComment()
		 * @generated
		 */
		EClass XML_COMMENT = eINSTANCE.getXMLComment();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentImpl <em>XML Document</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLDocumentImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLDocument()
		 * @generated
		 */
		EClass XML_DOCUMENT = eINSTANCE.getXMLDocument();

		/**
		 * The meta object literal for the '<em><b>Doctype</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XML_DOCUMENT__DOCTYPE = eINSTANCE.getXMLDocument_Doctype();

		/**
		 * The meta object literal for the '<em><b>Document URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_DOCUMENT__DOCUMENT_URI = eINSTANCE.getXMLDocument_DocumentURI();

		/**
		 * The meta object literal for the '<em><b>Input Encoding</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_DOCUMENT__INPUT_ENCODING = eINSTANCE.getXMLDocument_InputEncoding();

		/**
		 * The meta object literal for the '<em><b>Strict Error Checking</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_DOCUMENT__STRICT_ERROR_CHECKING = eINSTANCE.getXMLDocument_StrictErrorChecking();

		/**
		 * The meta object literal for the '<em><b>Xml Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_DOCUMENT__XML_VERSION = eINSTANCE.getXMLDocument_XmlVersion();

		/**
		 * The meta object literal for the '<em><b>Xml Encoding</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_DOCUMENT__XML_ENCODING = eINSTANCE.getXMLDocument_XmlEncoding();

		/**
		 * The meta object literal for the '<em><b>Xml Standalone</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_DOCUMENT__XML_STANDALONE = eINSTANCE.getXMLDocument_XmlStandalone();

		/**
		 * The meta object literal for the '<em><b>Document</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_DOCUMENT__DOCUMENT = eINSTANCE.getXMLDocument_Document();

		/**
		 * The meta object literal for the '<em><b>Forward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XML_DOCUMENT___FORWARD = eINSTANCE.getXMLDocument__Forward();

		/**
		 * The meta object literal for the '<em><b>Backward</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XML_DOCUMENT___BACKWARD = eINSTANCE.getXMLDocument__Backward();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentFragmentImpl <em>XML Document Fragment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLDocumentFragmentImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLDocumentFragment()
		 * @generated
		 */
		EClass XML_DOCUMENT_FRAGMENT = eINSTANCE.getXMLDocumentFragment();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentTypeImpl <em>XML Document Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLDocumentTypeImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLDocumentType()
		 * @generated
		 */
		EClass XML_DOCUMENT_TYPE = eINSTANCE.getXMLDocumentType();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLElementImpl <em>XML Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLElementImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLElement()
		 * @generated
		 */
		EClass XML_ELEMENT = eINSTANCE.getXMLElement();

		/**
		 * The meta object literal for the '<em><b>Tag Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_ELEMENT__TAG_NAME = eINSTANCE.getXMLElement_TagName();

		/**
		 * The meta object literal for the '<em><b>Local Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_ELEMENT__LOCAL_NAME = eINSTANCE.getXMLElement_LocalName();

		/**
		 * The meta object literal for the '<em><b>Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_ELEMENT__PREFIX = eINSTANCE.getXMLElement_Prefix();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLEntityImpl <em>XML Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLEntityImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLEntity()
		 * @generated
		 */
		EClass XML_ENTITY = eINSTANCE.getXMLEntity();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLEntityReferenceImpl <em>XML Entity Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLEntityReferenceImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLEntityReference()
		 * @generated
		 */
		EClass XML_ENTITY_REFERENCE = eINSTANCE.getXMLEntityReference();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLNotationImpl <em>XML Notation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLNotationImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLNotation()
		 * @generated
		 */
		EClass XML_NOTATION = eINSTANCE.getXMLNotation();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLProcessingInstructionImpl <em>XML Processing Instruction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLProcessingInstructionImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLProcessingInstruction()
		 * @generated
		 */
		EClass XML_PROCESSING_INSTRUCTION = eINSTANCE.getXMLProcessingInstruction();

		/**
		 * The meta object literal for the '<em><b>Node Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_PROCESSING_INSTRUCTION__NODE_VALUE = eINSTANCE.getXMLProcessingInstruction_NodeValue();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_PROCESSING_INSTRUCTION__DATA = eINSTANCE.getXMLProcessingInstruction_Data();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_PROCESSING_INSTRUCTION__TARGET = eINSTANCE.getXMLProcessingInstruction_Target();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.impl.XMLTextImpl <em>XML Text</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.impl.XMLTextImpl
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getXMLText()
		 * @generated
		 */
		EClass XML_TEXT = eINSTANCE.getXMLText();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.commonutil.eXML.NodeType <em>Node Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.commonutil.eXML.NodeType
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getNodeType()
		 * @generated
		 */
		EEnum NODE_TYPE = eINSTANCE.getNodeType();

		/**
		 * The meta object literal for the '<em>Java XML Node</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.w3c.dom.Node
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getJavaXMLNode()
		 * @generated
		 */
		EDataType JAVA_XML_NODE = eINSTANCE.getJavaXMLNode();

		/**
		 * The meta object literal for the '<em>Java XML Document</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.w3c.dom.Document
		 * @see edu.ustb.sei.commonutil.eXML.impl.EXMLPackageImpl#getJavaXMLDocument()
		 * @generated
		 */
		EDataType JAVA_XML_DOCUMENT = eINSTANCE.getJavaXMLDocument();

	}

} //EXMLPackage
