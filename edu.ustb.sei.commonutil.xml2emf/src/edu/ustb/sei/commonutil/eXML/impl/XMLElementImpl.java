/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.EXMLPackage;
import edu.ustb.sei.commonutil.eXML.XMLElement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XML Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLElementImpl#getTagName <em>Tag Name</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLElementImpl#getLocalName <em>Local Name</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLElementImpl#getPrefix <em>Prefix</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class XMLElementImpl extends XMLNodeImpl implements XMLElement {
	/**
	 * The default value of the '{@link #getTagName() <em>Tag Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTagName()
	 * @generated
	 * @ordered
	 */
	protected static final String TAG_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getTagName() <em>Tag Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTagName()
	 * @generated
	 * @ordered
	 */
	protected String tagName = TAG_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLocalName() <em>Local Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalName()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCAL_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getLocalName() <em>Local Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalName()
	 * @generated
	 * @ordered
	 */
	protected String localName = LOCAL_NAME_EDEFAULT;
	/**
	 * The default value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefix()
	 * @generated
	 * @ordered
	 */
	protected static final String PREFIX_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefix()
	 * @generated
	 * @ordered
	 */
	protected String prefix = PREFIX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EXMLPackage.Literals.XML_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTagName() {
		return tagName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setTagName(String newTagName) {
		String oldTagName = tagName;
		tagName = newTagName;
		
		nodeName = newTagName;
		updateQualifiedName(newTagName);
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_ELEMENT__TAG_NAME, oldTagName, tagName));
	}
	
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLocalName() {
		return localName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setLocalName(String newLocalName) {
		String oldLocalName = localName;
		localName = newLocalName;
		this.updateLocalName(newLocalName);
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_ELEMENT__LOCAL_NAME, oldLocalName, localName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setPrefix(String newPrefix) {
		String oldPrefix = prefix;
		prefix = newPrefix;
		this.updatePrefix(newPrefix);
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_ELEMENT__PREFIX, oldPrefix, prefix));
	}

	@Override
	public void setNodeName(String newNodeName) {
		
		super.setNodeName(newNodeName);
		
		tagName = newNodeName;
		this.updateQualifiedName(newNodeName);
	}
	
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EXMLPackage.XML_ELEMENT__TAG_NAME:
				return getTagName();
			case EXMLPackage.XML_ELEMENT__LOCAL_NAME:
				return getLocalName();
			case EXMLPackage.XML_ELEMENT__PREFIX:
				return getPrefix();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EXMLPackage.XML_ELEMENT__TAG_NAME:
				setTagName((String)newValue);
				return;
			case EXMLPackage.XML_ELEMENT__LOCAL_NAME:
				setLocalName((String)newValue);
				return;
			case EXMLPackage.XML_ELEMENT__PREFIX:
				setPrefix((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EXMLPackage.XML_ELEMENT__TAG_NAME:
				setTagName(TAG_NAME_EDEFAULT);
				return;
			case EXMLPackage.XML_ELEMENT__LOCAL_NAME:
				setLocalName(LOCAL_NAME_EDEFAULT);
				return;
			case EXMLPackage.XML_ELEMENT__PREFIX:
				setPrefix(PREFIX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EXMLPackage.XML_ELEMENT__TAG_NAME:
				return TAG_NAME_EDEFAULT == null ? tagName != null : !TAG_NAME_EDEFAULT.equals(tagName);
			case EXMLPackage.XML_ELEMENT__LOCAL_NAME:
				return LOCAL_NAME_EDEFAULT == null ? localName != null : !LOCAL_NAME_EDEFAULT.equals(localName);
			case EXMLPackage.XML_ELEMENT__PREFIX:
				return PREFIX_EDEFAULT == null ? prefix != null : !PREFIX_EDEFAULT.equals(prefix);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tagName: ");
		result.append(tagName);
		result.append(", localName: ");
		result.append(localName);
		result.append(", prefix: ");
		result.append(prefix);
		result.append(')');
		return result.toString();
	}
	
	void updateQualifiedName(String name) {
		if(name==null) name = "";
		
		int index = name.indexOf(':');
		if(index==-1) {
			prefix = null;
			localName = null;
		} else {
			String buf[] = name.split(":");
			prefix = buf[0];
			localName = buf[1];
		}
	}
	
	void updatePrefix(String str) {
		if(str==null||str.length()==0) {
			if(this.getLocalName()==null)
				return;
			else {
				this.setNodeName(this.getLocalName());
			}
		} else {
			if(this.getLocalName()==null)
				this.setNodeName(str+":"+this.getNodeName());
			else
				this.setNodeName(str+":"+this.getLocalName());
		}
	}
	
	void updateLocalName(String str) {
		if(str==null||str.length()==0) {
			if(this.getPrefix()==null)
				return;
			else {
				prefix = null;
			}
		} else {
			if(this.getPrefix()==null)
				this.setNodeName(str);
			else {
				this.setNodeName(this.getPrefix()+":"+str);
			}
		}
	}

} //XMLElementImpl
