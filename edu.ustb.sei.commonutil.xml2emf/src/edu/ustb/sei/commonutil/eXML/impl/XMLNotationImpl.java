/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.EXMLPackage;
import edu.ustb.sei.commonutil.eXML.XMLNotation;

import org.eclipse.emf.ecore.EClass;

/**
 * NOT Supported Now
 */
/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XML Notation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class XMLNotationImpl extends XMLNodeImpl implements XMLNotation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLNotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EXMLPackage.Literals.XML_NOTATION;
	}

} //XMLNotationImpl
