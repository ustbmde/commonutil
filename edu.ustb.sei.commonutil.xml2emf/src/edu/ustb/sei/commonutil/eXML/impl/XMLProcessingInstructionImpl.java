/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.EXMLPackage;
import edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XML Processing Instruction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLProcessingInstructionImpl#getNodeValue <em>Node Value</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLProcessingInstructionImpl#getData <em>Data</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLProcessingInstructionImpl#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class XMLProcessingInstructionImpl extends XMLNodeImpl implements XMLProcessingInstruction {
	/**
	 * The default value of the '{@link #getNodeValue() <em>Node Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeValue()
	 * @generated
	 * @ordered
	 */
	protected static final String NODE_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNodeValue() <em>Node Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeValue()
	 * @generated
	 * @ordered
	 */
	protected String nodeValue = NODE_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getData() <em>Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getData()
	 * @generated
	 * @ordered
	 */
	protected static final String DATA_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getData() <em>Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getData()
	 * @generated
	 * @ordered
	 */
	protected String data = DATA_EDEFAULT;

	/**
	 * The default value of the '{@link #getTarget() <em>Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected String target = TARGET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLProcessingInstructionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EXMLPackage.Literals.XML_PROCESSING_INSTRUCTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNodeValue() {
		return nodeValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setNodeValue(String newNodeValue) {
		String oldNodeValue = nodeValue;
		nodeValue = newNodeValue;
		data = newNodeValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_PROCESSING_INSTRUCTION__NODE_VALUE, oldNodeValue, nodeValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getData() {
		return data;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setData(String newData) {
		String oldData = data;
		data = newData;
		nodeValue = newData;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_PROCESSING_INSTRUCTION__DATA, oldData, data));
	}

	@Override
	public void setNodeName(String newNodeName) {
		super.setNodeName(newNodeName);
		target = newNodeName;
	}
	
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setTarget(String newTarget) {
		String oldTarget = target;
		target = newTarget;
		
		nodeName = newTarget;
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_PROCESSING_INSTRUCTION__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EXMLPackage.XML_PROCESSING_INSTRUCTION__NODE_VALUE:
				return getNodeValue();
			case EXMLPackage.XML_PROCESSING_INSTRUCTION__DATA:
				return getData();
			case EXMLPackage.XML_PROCESSING_INSTRUCTION__TARGET:
				return getTarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EXMLPackage.XML_PROCESSING_INSTRUCTION__NODE_VALUE:
				setNodeValue((String)newValue);
				return;
			case EXMLPackage.XML_PROCESSING_INSTRUCTION__DATA:
				setData((String)newValue);
				return;
			case EXMLPackage.XML_PROCESSING_INSTRUCTION__TARGET:
				setTarget((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EXMLPackage.XML_PROCESSING_INSTRUCTION__NODE_VALUE:
				setNodeValue(NODE_VALUE_EDEFAULT);
				return;
			case EXMLPackage.XML_PROCESSING_INSTRUCTION__DATA:
				setData(DATA_EDEFAULT);
				return;
			case EXMLPackage.XML_PROCESSING_INSTRUCTION__TARGET:
				setTarget(TARGET_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EXMLPackage.XML_PROCESSING_INSTRUCTION__NODE_VALUE:
				return NODE_VALUE_EDEFAULT == null ? nodeValue != null : !NODE_VALUE_EDEFAULT.equals(nodeValue);
			case EXMLPackage.XML_PROCESSING_INSTRUCTION__DATA:
				return DATA_EDEFAULT == null ? data != null : !DATA_EDEFAULT.equals(data);
			case EXMLPackage.XML_PROCESSING_INSTRUCTION__TARGET:
				return TARGET_EDEFAULT == null ? target != null : !TARGET_EDEFAULT.equals(target);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nodeValue: ");
		result.append(nodeValue);
		result.append(", data: ");
		result.append(data);
		result.append(", target: ");
		result.append(target);
		result.append(')');
		return result.toString();
	}
} //XMLProcessingInstructionImpl
