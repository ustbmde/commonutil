/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EXMLFactoryImpl extends EFactoryImpl implements EXMLFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EXMLFactory init() {
		try {
			EXMLFactory theEXMLFactory = (EXMLFactory)EPackage.Registry.INSTANCE.getEFactory(EXMLPackage.eNS_URI);
			if (theEXMLFactory != null) {
				return theEXMLFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new EXMLFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EXMLFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case EXMLPackage.XML_ATTR: return createXMLAttr();
			case EXMLPackage.XMLCDATA_SECTION: return createXMLCDATASection();
			case EXMLPackage.XML_COMMENT: return createXMLComment();
			case EXMLPackage.XML_DOCUMENT: return createXMLDocument();
			case EXMLPackage.XML_DOCUMENT_FRAGMENT: return createXMLDocumentFragment();
			case EXMLPackage.XML_DOCUMENT_TYPE: return createXMLDocumentType();
			case EXMLPackage.XML_ELEMENT: return createXMLElement();
			case EXMLPackage.XML_ENTITY: return createXMLEntity();
			case EXMLPackage.XML_ENTITY_REFERENCE: return createXMLEntityReference();
			case EXMLPackage.XML_NOTATION: return createXMLNotation();
			case EXMLPackage.XML_PROCESSING_INSTRUCTION: return createXMLProcessingInstruction();
			case EXMLPackage.XML_TEXT: return createXMLText();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case EXMLPackage.NODE_TYPE:
				return createNodeTypeFromString(eDataType, initialValue);
			case EXMLPackage.JAVA_XML_NODE:
				return createJavaXMLNodeFromString(eDataType, initialValue);
			case EXMLPackage.JAVA_XML_DOCUMENT:
				return createJavaXMLDocumentFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case EXMLPackage.NODE_TYPE:
				return convertNodeTypeToString(eDataType, instanceValue);
			case EXMLPackage.JAVA_XML_NODE:
				return convertJavaXMLNodeToString(eDataType, instanceValue);
			case EXMLPackage.JAVA_XML_DOCUMENT:
				return convertJavaXMLDocumentToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLAttr createXMLAttr() {
		XMLAttrImpl xmlAttr = new XMLAttrImpl();
		return xmlAttr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLCDATASection createXMLCDATASection() {
		XMLCDATASectionImpl xmlcdataSection = new XMLCDATASectionImpl();
		return xmlcdataSection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLComment createXMLComment() {
		XMLCommentImpl xmlComment = new XMLCommentImpl();
		return xmlComment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLDocument createXMLDocument() {
		XMLDocumentImpl xmlDocument = new XMLDocumentImpl();
		return xmlDocument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLDocumentFragment createXMLDocumentFragment() {
		XMLDocumentFragmentImpl xmlDocumentFragment = new XMLDocumentFragmentImpl();
		return xmlDocumentFragment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLDocumentType createXMLDocumentType() {
		XMLDocumentTypeImpl xmlDocumentType = new XMLDocumentTypeImpl();
		return xmlDocumentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLElement createXMLElement() {
		XMLElementImpl xmlElement = new XMLElementImpl();
		return xmlElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLEntity createXMLEntity() {
		XMLEntityImpl xmlEntity = new XMLEntityImpl();
		return xmlEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLEntityReference createXMLEntityReference() {
		XMLEntityReferenceImpl xmlEntityReference = new XMLEntityReferenceImpl();
		return xmlEntityReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLNotation createXMLNotation() {
		XMLNotationImpl xmlNotation = new XMLNotationImpl();
		return xmlNotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLProcessingInstruction createXMLProcessingInstruction() {
		XMLProcessingInstructionImpl xmlProcessingInstruction = new XMLProcessingInstructionImpl();
		return xmlProcessingInstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLText createXMLText() {
		XMLTextImpl xmlText = new XMLTextImpl();
		return xmlText;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeType createNodeTypeFromString(EDataType eDataType, String initialValue) {
		NodeType result = NodeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNodeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node createJavaXMLNodeFromString(EDataType eDataType, String initialValue) {
		return (Node)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertJavaXMLNodeToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Document createJavaXMLDocumentFromString(EDataType eDataType, String initialValue) {
		return (Document)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertJavaXMLDocumentToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EXMLPackage getEXMLPackage() {
		return (EXMLPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static EXMLPackage getPackage() {
		return EXMLPackage.eINSTANCE;
	}

} //EXMLFactoryImpl
