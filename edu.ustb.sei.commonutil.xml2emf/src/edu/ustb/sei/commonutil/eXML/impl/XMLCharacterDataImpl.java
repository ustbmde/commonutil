/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.EXMLPackage;
import edu.ustb.sei.commonutil.eXML.XMLCharacterData;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XML Character Data</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLCharacterDataImpl#getData <em>Data</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLCharacterDataImpl#getNodeValue <em>Node Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class XMLCharacterDataImpl extends XMLNodeImpl implements XMLCharacterData {
	/**
	 * The default value of the '{@link #getData() <em>Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getData()
	 * @generated
	 * @ordered
	 */
	protected static final String DATA_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getData() <em>Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getData()
	 * @generated
	 * @ordered
	 */
	protected String data = DATA_EDEFAULT;

	/**
	 * The default value of the '{@link #getNodeValue() <em>Node Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeValue()
	 * @generated
	 * @ordered
	 */
	protected static final String NODE_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNodeValue() <em>Node Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeValue()
	 * @generated
	 * @ordered
	 */
	protected String nodeValue = NODE_VALUE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLCharacterDataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EXMLPackage.Literals.XML_CHARACTER_DATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getData() {
		return data;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setData(String newData) {
		String oldData = data;
		data = newData;
		
		nodeValue = newData;
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_CHARACTER_DATA__DATA, oldData, data));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNodeValue() {
		return nodeValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setNodeValue(String newNodeValue) {
		String oldNodeValue = nodeValue;
		nodeValue = newNodeValue;
		
		data = newNodeValue;
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_CHARACTER_DATA__NODE_VALUE, oldNodeValue, nodeValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EXMLPackage.XML_CHARACTER_DATA__DATA:
				return getData();
			case EXMLPackage.XML_CHARACTER_DATA__NODE_VALUE:
				return getNodeValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EXMLPackage.XML_CHARACTER_DATA__DATA:
				setData((String)newValue);
				return;
			case EXMLPackage.XML_CHARACTER_DATA__NODE_VALUE:
				setNodeValue((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EXMLPackage.XML_CHARACTER_DATA__DATA:
				setData(DATA_EDEFAULT);
				return;
			case EXMLPackage.XML_CHARACTER_DATA__NODE_VALUE:
				setNodeValue(NODE_VALUE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EXMLPackage.XML_CHARACTER_DATA__DATA:
				return DATA_EDEFAULT == null ? data != null : !DATA_EDEFAULT.equals(data);
			case EXMLPackage.XML_CHARACTER_DATA__NODE_VALUE:
				return NODE_VALUE_EDEFAULT == null ? nodeValue != null : !NODE_VALUE_EDEFAULT.equals(nodeValue);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (data: ");
		result.append(data);
		result.append(", nodeValue: ");
		result.append(nodeValue);
		result.append(')');
		return result.toString();
	}
} //XMLCharacterDataImpl
