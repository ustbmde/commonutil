/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.EXMLPackage;
import edu.ustb.sei.commonutil.eXML.XMLDocumentFragment;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XML Document Fragment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class XMLDocumentFragmentImpl extends XMLNodeImpl implements XMLDocumentFragment {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected XMLDocumentFragmentImpl() {
		super();
		nodeName = "#document-fragment";
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EXMLPackage.Literals.XML_DOCUMENT_FRAGMENT;
	}

	@Override
	public String getNodeName() {
		return "#document-fragment";
	}

	@Override
	public void setNodeName(String newNodeName) {		
	}

} //XMLDocumentFragmentImpl
