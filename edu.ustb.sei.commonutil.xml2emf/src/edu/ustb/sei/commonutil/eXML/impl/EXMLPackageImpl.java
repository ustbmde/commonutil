/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.EXMLFactory;
import edu.ustb.sei.commonutil.eXML.EXMLPackage;
import edu.ustb.sei.commonutil.eXML.NodeType;
import edu.ustb.sei.commonutil.eXML.XMLAttr;
import edu.ustb.sei.commonutil.eXML.XMLCDATASection;
import edu.ustb.sei.commonutil.eXML.XMLCharacterData;
import edu.ustb.sei.commonutil.eXML.XMLComment;
import edu.ustb.sei.commonutil.eXML.XMLDocument;
import edu.ustb.sei.commonutil.eXML.XMLDocumentFragment;
import edu.ustb.sei.commonutil.eXML.XMLDocumentType;
import edu.ustb.sei.commonutil.eXML.XMLElement;
import edu.ustb.sei.commonutil.eXML.XMLEntity;
import edu.ustb.sei.commonutil.eXML.XMLEntityReference;
import edu.ustb.sei.commonutil.eXML.XMLNode;
import edu.ustb.sei.commonutil.eXML.XMLNotation;
import edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction;
import edu.ustb.sei.commonutil.eXML.XMLText;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EXMLPackageImpl extends EPackageImpl implements EXMLPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlAttrEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlcdataSectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlCharacterDataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlCommentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlDocumentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlDocumentFragmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlDocumentTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlEntityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlEntityReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlNotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlProcessingInstructionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlTextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum nodeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType javaXMLNodeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType javaXMLDocumentEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EXMLPackageImpl() {
		super(eNS_URI, EXMLFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link EXMLPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EXMLPackage init() {
		if (isInited) return (EXMLPackage)EPackage.Registry.INSTANCE.getEPackage(EXMLPackage.eNS_URI);

		// Obtain or create and register package
		EXMLPackageImpl theEXMLPackage = (EXMLPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof EXMLPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new EXMLPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theEXMLPackage.createPackageContents();

		// Initialize created meta-data
		theEXMLPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theEXMLPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(EXMLPackage.eNS_URI, theEXMLPackage);
		return theEXMLPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLNode() {
		return xmlNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLNode_BaseURI() {
		return (EAttribute)xmlNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXMLNode_ChildNodes() {
		return (EReference)xmlNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXMLNode_Attributes() {
		return (EReference)xmlNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLNode_NamespaceURI() {
		return (EAttribute)xmlNodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLNode_NodeName() {
		return (EAttribute)xmlNodeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLNode_NodeType() {
		return (EAttribute)xmlNodeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXMLNode_ParentNode() {
		return (EReference)xmlNodeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLAttr() {
		return xmlAttrEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLAttr_NodeValue() {
		return (EAttribute)xmlAttrEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLAttr_Name() {
		return (EAttribute)xmlAttrEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLAttr_Value() {
		return (EAttribute)xmlAttrEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLAttr_LocalName() {
		return (EAttribute)xmlAttrEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLAttr_Prefix() {
		return (EAttribute)xmlAttrEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLCDATASection() {
		return xmlcdataSectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLCharacterData() {
		return xmlCharacterDataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLCharacterData_Data() {
		return (EAttribute)xmlCharacterDataEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLCharacterData_NodeValue() {
		return (EAttribute)xmlCharacterDataEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLComment() {
		return xmlCommentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLDocument() {
		return xmlDocumentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXMLDocument_Doctype() {
		return (EReference)xmlDocumentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLDocument_DocumentURI() {
		return (EAttribute)xmlDocumentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLDocument_InputEncoding() {
		return (EAttribute)xmlDocumentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLDocument_StrictErrorChecking() {
		return (EAttribute)xmlDocumentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLDocument_XmlVersion() {
		return (EAttribute)xmlDocumentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLDocument_XmlEncoding() {
		return (EAttribute)xmlDocumentEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLDocument_XmlStandalone() {
		return (EAttribute)xmlDocumentEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLDocument_Document() {
		return (EAttribute)xmlDocumentEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXMLDocument__Forward() {
		return xmlDocumentEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXMLDocument__Backward() {
		return xmlDocumentEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLDocumentFragment() {
		return xmlDocumentFragmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLDocumentType() {
		return xmlDocumentTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLElement() {
		return xmlElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLElement_TagName() {
		return (EAttribute)xmlElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLElement_LocalName() {
		return (EAttribute)xmlElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLElement_Prefix() {
		return (EAttribute)xmlElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLEntity() {
		return xmlEntityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLEntityReference() {
		return xmlEntityReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLNotation() {
		return xmlNotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLProcessingInstruction() {
		return xmlProcessingInstructionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLProcessingInstruction_NodeValue() {
		return (EAttribute)xmlProcessingInstructionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLProcessingInstruction_Data() {
		return (EAttribute)xmlProcessingInstructionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLProcessingInstruction_Target() {
		return (EAttribute)xmlProcessingInstructionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLText() {
		return xmlTextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getNodeType() {
		return nodeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getJavaXMLNode() {
		return javaXMLNodeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getJavaXMLDocument() {
		return javaXMLDocumentEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EXMLFactory getEXMLFactory() {
		return (EXMLFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		xmlNodeEClass = createEClass(XML_NODE);
		createEAttribute(xmlNodeEClass, XML_NODE__BASE_URI);
		createEReference(xmlNodeEClass, XML_NODE__CHILD_NODES);
		createEReference(xmlNodeEClass, XML_NODE__ATTRIBUTES);
		createEAttribute(xmlNodeEClass, XML_NODE__NAMESPACE_URI);
		createEAttribute(xmlNodeEClass, XML_NODE__NODE_NAME);
		createEAttribute(xmlNodeEClass, XML_NODE__NODE_TYPE);
		createEReference(xmlNodeEClass, XML_NODE__PARENT_NODE);

		xmlAttrEClass = createEClass(XML_ATTR);
		createEAttribute(xmlAttrEClass, XML_ATTR__NODE_VALUE);
		createEAttribute(xmlAttrEClass, XML_ATTR__NAME);
		createEAttribute(xmlAttrEClass, XML_ATTR__VALUE);
		createEAttribute(xmlAttrEClass, XML_ATTR__LOCAL_NAME);
		createEAttribute(xmlAttrEClass, XML_ATTR__PREFIX);

		xmlcdataSectionEClass = createEClass(XMLCDATA_SECTION);

		xmlCharacterDataEClass = createEClass(XML_CHARACTER_DATA);
		createEAttribute(xmlCharacterDataEClass, XML_CHARACTER_DATA__DATA);
		createEAttribute(xmlCharacterDataEClass, XML_CHARACTER_DATA__NODE_VALUE);

		xmlCommentEClass = createEClass(XML_COMMENT);

		xmlDocumentEClass = createEClass(XML_DOCUMENT);
		createEReference(xmlDocumentEClass, XML_DOCUMENT__DOCTYPE);
		createEAttribute(xmlDocumentEClass, XML_DOCUMENT__DOCUMENT_URI);
		createEAttribute(xmlDocumentEClass, XML_DOCUMENT__INPUT_ENCODING);
		createEAttribute(xmlDocumentEClass, XML_DOCUMENT__STRICT_ERROR_CHECKING);
		createEAttribute(xmlDocumentEClass, XML_DOCUMENT__XML_VERSION);
		createEAttribute(xmlDocumentEClass, XML_DOCUMENT__XML_ENCODING);
		createEAttribute(xmlDocumentEClass, XML_DOCUMENT__XML_STANDALONE);
		createEAttribute(xmlDocumentEClass, XML_DOCUMENT__DOCUMENT);
		createEOperation(xmlDocumentEClass, XML_DOCUMENT___FORWARD);
		createEOperation(xmlDocumentEClass, XML_DOCUMENT___BACKWARD);

		xmlDocumentFragmentEClass = createEClass(XML_DOCUMENT_FRAGMENT);

		xmlDocumentTypeEClass = createEClass(XML_DOCUMENT_TYPE);

		xmlElementEClass = createEClass(XML_ELEMENT);
		createEAttribute(xmlElementEClass, XML_ELEMENT__TAG_NAME);
		createEAttribute(xmlElementEClass, XML_ELEMENT__LOCAL_NAME);
		createEAttribute(xmlElementEClass, XML_ELEMENT__PREFIX);

		xmlEntityEClass = createEClass(XML_ENTITY);

		xmlEntityReferenceEClass = createEClass(XML_ENTITY_REFERENCE);

		xmlNotationEClass = createEClass(XML_NOTATION);

		xmlProcessingInstructionEClass = createEClass(XML_PROCESSING_INSTRUCTION);
		createEAttribute(xmlProcessingInstructionEClass, XML_PROCESSING_INSTRUCTION__NODE_VALUE);
		createEAttribute(xmlProcessingInstructionEClass, XML_PROCESSING_INSTRUCTION__DATA);
		createEAttribute(xmlProcessingInstructionEClass, XML_PROCESSING_INSTRUCTION__TARGET);

		xmlTextEClass = createEClass(XML_TEXT);

		// Create enums
		nodeTypeEEnum = createEEnum(NODE_TYPE);

		// Create data types
		javaXMLNodeEDataType = createEDataType(JAVA_XML_NODE);
		javaXMLDocumentEDataType = createEDataType(JAVA_XML_DOCUMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		xmlAttrEClass.getESuperTypes().add(this.getXMLNode());
		xmlcdataSectionEClass.getESuperTypes().add(this.getXMLText());
		xmlCharacterDataEClass.getESuperTypes().add(this.getXMLNode());
		xmlCommentEClass.getESuperTypes().add(this.getXMLCharacterData());
		xmlDocumentEClass.getESuperTypes().add(this.getXMLNode());
		xmlDocumentFragmentEClass.getESuperTypes().add(this.getXMLNode());
		xmlDocumentTypeEClass.getESuperTypes().add(this.getXMLNode());
		xmlElementEClass.getESuperTypes().add(this.getXMLNode());
		xmlEntityEClass.getESuperTypes().add(this.getXMLNode());
		xmlEntityReferenceEClass.getESuperTypes().add(this.getXMLNode());
		xmlNotationEClass.getESuperTypes().add(this.getXMLNode());
		xmlProcessingInstructionEClass.getESuperTypes().add(this.getXMLNode());
		xmlTextEClass.getESuperTypes().add(this.getXMLCharacterData());

		// Initialize classes, features, and operations; add parameters
		initEClass(xmlNodeEClass, XMLNode.class, "XMLNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getXMLNode_BaseURI(), ecorePackage.getEString(), "baseURI", null, 0, 1, XMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXMLNode_ChildNodes(), this.getXMLNode(), this.getXMLNode_ParentNode(), "childNodes", null, 0, -1, XMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXMLNode_Attributes(), this.getXMLAttr(), null, "attributes", null, 0, -1, XMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLNode_NamespaceURI(), ecorePackage.getEString(), "namespaceURI", null, 0, 1, XMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLNode_NodeName(), ecorePackage.getEString(), "nodeName", null, 0, 1, XMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLNode_NodeType(), this.getNodeType(), "nodeType", null, 0, 1, XMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXMLNode_ParentNode(), this.getXMLNode(), this.getXMLNode_ChildNodes(), "parentNode", null, 0, 1, XMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xmlAttrEClass, XMLAttr.class, "XMLAttr", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getXMLAttr_NodeValue(), ecorePackage.getEString(), "nodeValue", null, 0, 1, XMLAttr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLAttr_Name(), ecorePackage.getEString(), "name", null, 0, 1, XMLAttr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLAttr_Value(), ecorePackage.getEString(), "value", null, 0, 1, XMLAttr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLAttr_LocalName(), ecorePackage.getEString(), "localName", null, 0, 1, XMLAttr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLAttr_Prefix(), ecorePackage.getEString(), "prefix", null, 0, 1, XMLAttr.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xmlcdataSectionEClass, XMLCDATASection.class, "XMLCDATASection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xmlCharacterDataEClass, XMLCharacterData.class, "XMLCharacterData", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getXMLCharacterData_Data(), ecorePackage.getEString(), "data", null, 0, 1, XMLCharacterData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLCharacterData_NodeValue(), ecorePackage.getEString(), "nodeValue", null, 0, 1, XMLCharacterData.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xmlCommentEClass, XMLComment.class, "XMLComment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xmlDocumentEClass, XMLDocument.class, "XMLDocument", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXMLDocument_Doctype(), this.getXMLDocumentType(), null, "doctype", null, 0, 1, XMLDocument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLDocument_DocumentURI(), ecorePackage.getEString(), "documentURI", null, 0, 1, XMLDocument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLDocument_InputEncoding(), ecorePackage.getEString(), "inputEncoding", null, 0, 1, XMLDocument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLDocument_StrictErrorChecking(), ecorePackage.getEBoolean(), "strictErrorChecking", "false", 0, 1, XMLDocument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLDocument_XmlVersion(), ecorePackage.getEString(), "xmlVersion", "1.0", 0, 1, XMLDocument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLDocument_XmlEncoding(), ecorePackage.getEString(), "xmlEncoding", null, 0, 1, XMLDocument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLDocument_XmlStandalone(), ecorePackage.getEBoolean(), "xmlStandalone", "true", 0, 1, XMLDocument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLDocument_Document(), this.getJavaXMLDocument(), "document", null, 0, 1, XMLDocument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getXMLDocument__Forward(), null, "forward", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getXMLDocument__Backward(), null, "backward", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(xmlDocumentFragmentEClass, XMLDocumentFragment.class, "XMLDocumentFragment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xmlDocumentTypeEClass, XMLDocumentType.class, "XMLDocumentType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xmlElementEClass, XMLElement.class, "XMLElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getXMLElement_TagName(), ecorePackage.getEString(), "tagName", null, 0, 1, XMLElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLElement_LocalName(), ecorePackage.getEString(), "localName", null, 0, 1, XMLElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLElement_Prefix(), ecorePackage.getEString(), "prefix", null, 0, 1, XMLElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xmlEntityEClass, XMLEntity.class, "XMLEntity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xmlEntityReferenceEClass, XMLEntityReference.class, "XMLEntityReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xmlNotationEClass, XMLNotation.class, "XMLNotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(xmlProcessingInstructionEClass, XMLProcessingInstruction.class, "XMLProcessingInstruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getXMLProcessingInstruction_NodeValue(), ecorePackage.getEString(), "nodeValue", null, 0, 1, XMLProcessingInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLProcessingInstruction_Data(), ecorePackage.getEString(), "data", null, 0, 1, XMLProcessingInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLProcessingInstruction_Target(), ecorePackage.getEString(), "target", null, 0, 1, XMLProcessingInstruction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xmlTextEClass, XMLText.class, "XMLText", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(nodeTypeEEnum, NodeType.class, "NodeType");
		addEEnumLiteral(nodeTypeEEnum, NodeType.ATTRIBUTE_NODE);
		addEEnumLiteral(nodeTypeEEnum, NodeType.CDATA_SECTION_NODE);
		addEEnumLiteral(nodeTypeEEnum, NodeType.COMMENT_NODE);
		addEEnumLiteral(nodeTypeEEnum, NodeType.DOCUMENT_FRAGMENT_NODE);
		addEEnumLiteral(nodeTypeEEnum, NodeType.DOCUMENT_NODE);
		addEEnumLiteral(nodeTypeEEnum, NodeType.DOCUMENT_TYPE_NODE);
		addEEnumLiteral(nodeTypeEEnum, NodeType.ELEMENT_NODE);
		addEEnumLiteral(nodeTypeEEnum, NodeType.ENTITY_NODE);
		addEEnumLiteral(nodeTypeEEnum, NodeType.ENTITY_REFERENCE_NODE);
		addEEnumLiteral(nodeTypeEEnum, NodeType.NOTATION_NODE);
		addEEnumLiteral(nodeTypeEEnum, NodeType.PROCESSING_INSTRUCTION_NODE);
		addEEnumLiteral(nodeTypeEEnum, NodeType.TEXT_NODE);

		// Initialize data types
		initEDataType(javaXMLNodeEDataType, Node.class, "JavaXMLNode", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(javaXMLDocumentEDataType, Document.class, "JavaXMLDocument", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //EXMLPackageImpl
