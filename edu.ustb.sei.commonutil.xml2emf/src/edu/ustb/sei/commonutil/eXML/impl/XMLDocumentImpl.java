/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.EXMLPackage;
import edu.ustb.sei.commonutil.eXML.XMLDocument;
import edu.ustb.sei.commonutil.eXML.XMLDocumentType;
import edu.ustb.sei.commonutil.eXML.XMLNode;
import edu.ustb.sei.commonutil.eXML.util.ConversionHelper;
import edu.ustb.sei.commonutil.util.BidirectionalMap;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XML Document</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentImpl#getDoctype <em>Doctype</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentImpl#getDocumentURI <em>Document URI</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentImpl#getInputEncoding <em>Input Encoding</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentImpl#isStrictErrorChecking <em>Strict Error Checking</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentImpl#getXmlVersion <em>Xml Version</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentImpl#getXmlEncoding <em>Xml Encoding</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentImpl#isXmlStandalone <em>Xml Standalone</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLDocumentImpl#getDocument <em>Document</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class XMLDocumentImpl extends XMLNodeImpl implements XMLDocument {
	/**
	 * The cached value of the '{@link #getDoctype() <em>Doctype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoctype()
	 * @generated
	 * @ordered
	 */
	protected XMLDocumentType doctype;
	/**
	 * The default value of the '{@link #getDocumentURI() <em>Document URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentURI()
	 * @generated
	 * @ordered
	 */
	protected static final String DOCUMENT_URI_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getDocumentURI() <em>Document URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentURI()
	 * @generated
	 * @ordered
	 */
	protected String documentURI = DOCUMENT_URI_EDEFAULT;
	/**
	 * The default value of the '{@link #getInputEncoding() <em>Input Encoding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputEncoding()
	 * @generated
	 * @ordered
	 */
	protected static final String INPUT_ENCODING_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getInputEncoding() <em>Input Encoding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputEncoding()
	 * @generated
	 * @ordered
	 */
	protected String inputEncoding = INPUT_ENCODING_EDEFAULT;
	/**
	 * The default value of the '{@link #isStrictErrorChecking() <em>Strict Error Checking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStrictErrorChecking()
	 * @generated
	 * @ordered
	 */
	protected static final boolean STRICT_ERROR_CHECKING_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isStrictErrorChecking() <em>Strict Error Checking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isStrictErrorChecking()
	 * @generated
	 * @ordered
	 */
	protected boolean strictErrorChecking = STRICT_ERROR_CHECKING_EDEFAULT;
	/**
	 * The default value of the '{@link #getXmlVersion() <em>Xml Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXmlVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String XML_VERSION_EDEFAULT = "1.0";
	/**
	 * The cached value of the '{@link #getXmlVersion() <em>Xml Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXmlVersion()
	 * @generated
	 * @ordered
	 */
	protected String xmlVersion = XML_VERSION_EDEFAULT;
	/**
	 * The default value of the '{@link #getXmlEncoding() <em>Xml Encoding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXmlEncoding()
	 * @generated
	 * @ordered
	 */
	protected static final String XML_ENCODING_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getXmlEncoding() <em>Xml Encoding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXmlEncoding()
	 * @generated
	 * @ordered
	 */
	protected String xmlEncoding = XML_ENCODING_EDEFAULT;
	/**
	 * The default value of the '{@link #isXmlStandalone() <em>Xml Standalone</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isXmlStandalone()
	 * @generated
	 * @ordered
	 */
	protected static final boolean XML_STANDALONE_EDEFAULT = true;
	/**
	 * The cached value of the '{@link #isXmlStandalone() <em>Xml Standalone</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isXmlStandalone()
	 * @generated
	 * @ordered
	 */
	protected boolean xmlStandalone = XML_STANDALONE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDocument() <em>Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocument()
	 * @generated
	 * @ordered
	 */
	protected static final Document DOCUMENT_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getDocument() <em>Document</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocument()
	 * @generated
	 * @ordered
	 */
	protected Document document = DOCUMENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected XMLDocumentImpl() {
		super();
		nodeName = "#document";
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EXMLPackage.Literals.XML_DOCUMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLDocumentType getDoctype() {
		return doctype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDoctype(XMLDocumentType newDoctype, NotificationChain msgs) {
		XMLDocumentType oldDoctype = doctype;
		doctype = newDoctype;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_DOCUMENT__DOCTYPE, oldDoctype, newDoctype);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDoctype(XMLDocumentType newDoctype) {
		if (newDoctype != doctype) {
			NotificationChain msgs = null;
			if (doctype != null)
				msgs = ((InternalEObject)doctype).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - EXMLPackage.XML_DOCUMENT__DOCTYPE, null, msgs);
			if (newDoctype != null)
				msgs = ((InternalEObject)newDoctype).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - EXMLPackage.XML_DOCUMENT__DOCTYPE, null, msgs);
			msgs = basicSetDoctype(newDoctype, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_DOCUMENT__DOCTYPE, newDoctype, newDoctype));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDocumentURI() {
		return documentURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDocumentURI(String newDocumentURI) {
		String oldDocumentURI = documentURI;
		documentURI = newDocumentURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_DOCUMENT__DOCUMENT_URI, oldDocumentURI, documentURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInputEncoding() {
		return inputEncoding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputEncoding(String newInputEncoding) {
		String oldInputEncoding = inputEncoding;
		inputEncoding = newInputEncoding;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_DOCUMENT__INPUT_ENCODING, oldInputEncoding, inputEncoding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isStrictErrorChecking() {
		return strictErrorChecking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStrictErrorChecking(boolean newStrictErrorChecking) {
		boolean oldStrictErrorChecking = strictErrorChecking;
		strictErrorChecking = newStrictErrorChecking;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_DOCUMENT__STRICT_ERROR_CHECKING, oldStrictErrorChecking, strictErrorChecking));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getXmlVersion() {
		return xmlVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setXmlVersion(String newXmlVersion) {
		String oldXmlVersion = xmlVersion;
		xmlVersion = newXmlVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_DOCUMENT__XML_VERSION, oldXmlVersion, xmlVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getXmlEncoding() {
		return xmlEncoding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setXmlEncoding(String newXmlEncoding) {
		String oldXmlEncoding = xmlEncoding;
		xmlEncoding = newXmlEncoding;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_DOCUMENT__XML_ENCODING, oldXmlEncoding, xmlEncoding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isXmlStandalone() {
		return xmlStandalone;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setXmlStandalone(boolean newXmlStandalone) {
		boolean oldXmlStandalone = xmlStandalone;
		xmlStandalone = newXmlStandalone;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_DOCUMENT__XML_STANDALONE, oldXmlStandalone, xmlStandalone));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Document getDocument() {
		return document;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDocument(Document newDocument) {
		Document oldDocument = document;
		document = newDocument;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_DOCUMENT__DOCUMENT, oldDocument, document));
	}
	
	
	protected BidirectionalMap<Node,XMLNode> map = new BidirectionalMap<Node,XMLNode>();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void forward() {
		Document d = getDocument();
		ConversionHelper.forwardDocument(d,this,map);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void backward() {
		Document d = getDocument();
		ConversionHelper.backwardDocument(this, d, map);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EXMLPackage.XML_DOCUMENT__DOCTYPE:
				return basicSetDoctype(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EXMLPackage.XML_DOCUMENT__DOCTYPE:
				return getDoctype();
			case EXMLPackage.XML_DOCUMENT__DOCUMENT_URI:
				return getDocumentURI();
			case EXMLPackage.XML_DOCUMENT__INPUT_ENCODING:
				return getInputEncoding();
			case EXMLPackage.XML_DOCUMENT__STRICT_ERROR_CHECKING:
				return isStrictErrorChecking();
			case EXMLPackage.XML_DOCUMENT__XML_VERSION:
				return getXmlVersion();
			case EXMLPackage.XML_DOCUMENT__XML_ENCODING:
				return getXmlEncoding();
			case EXMLPackage.XML_DOCUMENT__XML_STANDALONE:
				return isXmlStandalone();
			case EXMLPackage.XML_DOCUMENT__DOCUMENT:
				return getDocument();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EXMLPackage.XML_DOCUMENT__DOCTYPE:
				setDoctype((XMLDocumentType)newValue);
				return;
			case EXMLPackage.XML_DOCUMENT__DOCUMENT_URI:
				setDocumentURI((String)newValue);
				return;
			case EXMLPackage.XML_DOCUMENT__INPUT_ENCODING:
				setInputEncoding((String)newValue);
				return;
			case EXMLPackage.XML_DOCUMENT__STRICT_ERROR_CHECKING:
				setStrictErrorChecking((Boolean)newValue);
				return;
			case EXMLPackage.XML_DOCUMENT__XML_VERSION:
				setXmlVersion((String)newValue);
				return;
			case EXMLPackage.XML_DOCUMENT__XML_ENCODING:
				setXmlEncoding((String)newValue);
				return;
			case EXMLPackage.XML_DOCUMENT__XML_STANDALONE:
				setXmlStandalone((Boolean)newValue);
				return;
			case EXMLPackage.XML_DOCUMENT__DOCUMENT:
				setDocument((Document)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EXMLPackage.XML_DOCUMENT__DOCTYPE:
				setDoctype((XMLDocumentType)null);
				return;
			case EXMLPackage.XML_DOCUMENT__DOCUMENT_URI:
				setDocumentURI(DOCUMENT_URI_EDEFAULT);
				return;
			case EXMLPackage.XML_DOCUMENT__INPUT_ENCODING:
				setInputEncoding(INPUT_ENCODING_EDEFAULT);
				return;
			case EXMLPackage.XML_DOCUMENT__STRICT_ERROR_CHECKING:
				setStrictErrorChecking(STRICT_ERROR_CHECKING_EDEFAULT);
				return;
			case EXMLPackage.XML_DOCUMENT__XML_VERSION:
				setXmlVersion(XML_VERSION_EDEFAULT);
				return;
			case EXMLPackage.XML_DOCUMENT__XML_ENCODING:
				setXmlEncoding(XML_ENCODING_EDEFAULT);
				return;
			case EXMLPackage.XML_DOCUMENT__XML_STANDALONE:
				setXmlStandalone(XML_STANDALONE_EDEFAULT);
				return;
			case EXMLPackage.XML_DOCUMENT__DOCUMENT:
				setDocument(DOCUMENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EXMLPackage.XML_DOCUMENT__DOCTYPE:
				return doctype != null;
			case EXMLPackage.XML_DOCUMENT__DOCUMENT_URI:
				return DOCUMENT_URI_EDEFAULT == null ? documentURI != null : !DOCUMENT_URI_EDEFAULT.equals(documentURI);
			case EXMLPackage.XML_DOCUMENT__INPUT_ENCODING:
				return INPUT_ENCODING_EDEFAULT == null ? inputEncoding != null : !INPUT_ENCODING_EDEFAULT.equals(inputEncoding);
			case EXMLPackage.XML_DOCUMENT__STRICT_ERROR_CHECKING:
				return strictErrorChecking != STRICT_ERROR_CHECKING_EDEFAULT;
			case EXMLPackage.XML_DOCUMENT__XML_VERSION:
				return XML_VERSION_EDEFAULT == null ? xmlVersion != null : !XML_VERSION_EDEFAULT.equals(xmlVersion);
			case EXMLPackage.XML_DOCUMENT__XML_ENCODING:
				return XML_ENCODING_EDEFAULT == null ? xmlEncoding != null : !XML_ENCODING_EDEFAULT.equals(xmlEncoding);
			case EXMLPackage.XML_DOCUMENT__XML_STANDALONE:
				return xmlStandalone != XML_STANDALONE_EDEFAULT;
			case EXMLPackage.XML_DOCUMENT__DOCUMENT:
				return DOCUMENT_EDEFAULT == null ? document != null : !DOCUMENT_EDEFAULT.equals(document);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case EXMLPackage.XML_DOCUMENT___FORWARD:
				forward();
				return null;
			case EXMLPackage.XML_DOCUMENT___BACKWARD:
				backward();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (documentURI: ");
		result.append(documentURI);
		result.append(", inputEncoding: ");
		result.append(inputEncoding);
		result.append(", strictErrorChecking: ");
		result.append(strictErrorChecking);
		result.append(", xmlVersion: ");
		result.append(xmlVersion);
		result.append(", xmlEncoding: ");
		result.append(xmlEncoding);
		result.append(", xmlStandalone: ");
		result.append(xmlStandalone);
		result.append(", document: ");
		result.append(document);
		result.append(')');
		return result.toString();
	}

	@Override
	public String getNodeName() {
		return "#document";
	}

	@Override
	public void setNodeName(String newNodeName) {
	}

} //XMLDocumentImpl
