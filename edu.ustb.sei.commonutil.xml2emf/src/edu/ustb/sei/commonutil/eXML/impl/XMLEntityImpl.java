/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.EXMLPackage;
import edu.ustb.sei.commonutil.eXML.XMLEntity;
import org.eclipse.emf.ecore.EClass;

/**
 * NOT Supported Now
 */
/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XML Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class XMLEntityImpl extends XMLNodeImpl implements XMLEntity {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLEntityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EXMLPackage.Literals.XML_ENTITY;
	}

} //XMLEntityImpl
