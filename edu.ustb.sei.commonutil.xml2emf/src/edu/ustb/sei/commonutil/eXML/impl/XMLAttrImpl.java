/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.EXMLPackage;
import edu.ustb.sei.commonutil.eXML.XMLAttr;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XML Attr</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLAttrImpl#getNodeValue <em>Node Value</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLAttrImpl#getName <em>Name</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLAttrImpl#getValue <em>Value</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLAttrImpl#getLocalName <em>Local Name</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.impl.XMLAttrImpl#getPrefix <em>Prefix</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class XMLAttrImpl extends XMLNodeImpl implements XMLAttr {
	/**
	 * The default value of the '{@link #getNodeValue() <em>Node Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeValue()
	 * @generated
	 * @ordered
	 */
	protected static final String NODE_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNodeValue() <em>Node Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodeValue()
	 * @generated
	 * @ordered
	 */
	protected String nodeValue = NODE_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLocalName() <em>Local Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalName()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCAL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocalName() <em>Local Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalName()
	 * @generated
	 * @ordered
	 */
	protected String localName = LOCAL_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefix()
	 * @generated
	 * @ordered
	 */
	protected static final String PREFIX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefix()
	 * @generated
	 * @ordered
	 */
	protected String prefix = PREFIX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLAttrImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EXMLPackage.Literals.XML_ATTR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNodeValue() {
		return nodeValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setNodeValue(String newNodeValue) {
		String oldNodeValue = nodeValue;
		nodeValue = newNodeValue;
		
		value = newNodeValue;
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_ATTR__NODE_VALUE, oldNodeValue, nodeValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		
		nodeName = newName;
		updateQualifiedName(newName);
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_ATTR__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		
		nodeValue = newValue;
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_ATTR__VALUE, oldValue, value));
	}
	
	void updateQualifiedName(String name) {
		if(name==null) name = "";
		
		int index = name.indexOf(':');
		if(index==-1) {
			prefix = null;
			localName = name;
		} else {
			String buf[] = name.split(":");
			prefix = buf[0];
			localName = buf[1];
		}
	}
	
	void updatePrefix(String str) {
		if(str==null||str.length()==1) {
			this.setNodeName(this.getLocalName());
		} else {
			this.setNodeName(str+":"+this.getLocalName());
		}
	}
	
	void updateLocalName(String str) {
		if(str==null||str.length()==1) {
			return;
		} else {
			this.setNodeName(this.getPrefix()+":"+str);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLocalName() {
		return localName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setLocalName(String newLocalName) {
		String oldLocalName = localName;
		localName = newLocalName;
		
		updateLocalName(newLocalName);
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_ATTR__LOCAL_NAME, oldLocalName, localName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setPrefix(String newPrefix) {
		String oldPrefix = prefix;
		prefix = newPrefix;
		
		this.updatePrefix(newPrefix);
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EXMLPackage.XML_ATTR__PREFIX, oldPrefix, prefix));
	}

	@Override
	public void setNodeName(String newNodeName) {
		
		super.setNodeName(newNodeName);
		
		name = newNodeName;
		
		this.updateQualifiedName(newNodeName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EXMLPackage.XML_ATTR__NODE_VALUE:
				return getNodeValue();
			case EXMLPackage.XML_ATTR__NAME:
				return getName();
			case EXMLPackage.XML_ATTR__VALUE:
				return getValue();
			case EXMLPackage.XML_ATTR__LOCAL_NAME:
				return getLocalName();
			case EXMLPackage.XML_ATTR__PREFIX:
				return getPrefix();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EXMLPackage.XML_ATTR__NODE_VALUE:
				setNodeValue((String)newValue);
				return;
			case EXMLPackage.XML_ATTR__NAME:
				setName((String)newValue);
				return;
			case EXMLPackage.XML_ATTR__VALUE:
				setValue((String)newValue);
				return;
			case EXMLPackage.XML_ATTR__LOCAL_NAME:
				setLocalName((String)newValue);
				return;
			case EXMLPackage.XML_ATTR__PREFIX:
				setPrefix((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EXMLPackage.XML_ATTR__NODE_VALUE:
				setNodeValue(NODE_VALUE_EDEFAULT);
				return;
			case EXMLPackage.XML_ATTR__NAME:
				setName(NAME_EDEFAULT);
				return;
			case EXMLPackage.XML_ATTR__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case EXMLPackage.XML_ATTR__LOCAL_NAME:
				setLocalName(LOCAL_NAME_EDEFAULT);
				return;
			case EXMLPackage.XML_ATTR__PREFIX:
				setPrefix(PREFIX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EXMLPackage.XML_ATTR__NODE_VALUE:
				return NODE_VALUE_EDEFAULT == null ? nodeValue != null : !NODE_VALUE_EDEFAULT.equals(nodeValue);
			case EXMLPackage.XML_ATTR__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case EXMLPackage.XML_ATTR__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case EXMLPackage.XML_ATTR__LOCAL_NAME:
				return LOCAL_NAME_EDEFAULT == null ? localName != null : !LOCAL_NAME_EDEFAULT.equals(localName);
			case EXMLPackage.XML_ATTR__PREFIX:
				return PREFIX_EDEFAULT == null ? prefix != null : !PREFIX_EDEFAULT.equals(prefix);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nodeValue: ");
		result.append(nodeValue);
		result.append(", name: ");
		result.append(name);
		result.append(", value: ");
		result.append(value);
		result.append(", localName: ");
		result.append(localName);
		result.append(", prefix: ");
		result.append(prefix);
		result.append(')');
		return result.toString();
	}

} //XMLAttrImpl
