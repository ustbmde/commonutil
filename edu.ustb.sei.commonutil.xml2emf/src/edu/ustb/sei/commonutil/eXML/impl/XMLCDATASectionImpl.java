/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.EXMLPackage;
import edu.ustb.sei.commonutil.eXML.XMLCDATASection;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XMLCDATA Section</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class XMLCDATASectionImpl extends XMLTextImpl implements XMLCDATASection {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected XMLCDATASectionImpl() {
		super();
		nodeName = "#cdata-section";
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EXMLPackage.Literals.XMLCDATA_SECTION;
	}

	@Override
	public String getNodeName() {
		return "#cdata-section";
	}

	@Override
	public void setNodeName(String newNodeName) {
	}

	
	
} //XMLCDATASectionImpl
