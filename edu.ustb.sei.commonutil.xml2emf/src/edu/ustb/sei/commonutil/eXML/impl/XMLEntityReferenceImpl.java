/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.EXMLPackage;
import edu.ustb.sei.commonutil.eXML.XMLEntityReference;

import org.eclipse.emf.ecore.EClass;

/**
 * NOT Supported Now
 */
/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XML Entity Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class XMLEntityReferenceImpl extends XMLNodeImpl implements XMLEntityReference {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLEntityReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EXMLPackage.Literals.XML_ENTITY_REFERENCE;
	}

} //XMLEntityReferenceImpl
