/**
 */
package edu.ustb.sei.commonutil.eXML.impl;

import edu.ustb.sei.commonutil.eXML.EXMLPackage;
import edu.ustb.sei.commonutil.eXML.XMLDocumentType;

import org.eclipse.emf.ecore.EClass;

/**
 * NOT Supported Now
 */
/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XML Document Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class XMLDocumentTypeImpl extends XMLNodeImpl implements XMLDocumentType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLDocumentTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EXMLPackage.Literals.XML_DOCUMENT_TYPE;
	}

} //XMLDocumentTypeImpl
