/**
 */
package edu.ustb.sei.commonutil.eXML;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage
 * @generated
 */
public interface EXMLFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EXMLFactory eINSTANCE = edu.ustb.sei.commonutil.eXML.impl.EXMLFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>XML Attr</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XML Attr</em>'.
	 * @generated
	 */
	XMLAttr createXMLAttr();

	/**
	 * Returns a new object of class '<em>XMLCDATA Section</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XMLCDATA Section</em>'.
	 * @generated
	 */
	XMLCDATASection createXMLCDATASection();

	/**
	 * Returns a new object of class '<em>XML Comment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XML Comment</em>'.
	 * @generated
	 */
	XMLComment createXMLComment();

	/**
	 * Returns a new object of class '<em>XML Document</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XML Document</em>'.
	 * @generated
	 */
	XMLDocument createXMLDocument();

	/**
	 * Returns a new object of class '<em>XML Document Fragment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XML Document Fragment</em>'.
	 * @generated
	 */
	XMLDocumentFragment createXMLDocumentFragment();

	/**
	 * Returns a new object of class '<em>XML Document Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XML Document Type</em>'.
	 * @generated
	 */
	XMLDocumentType createXMLDocumentType();

	/**
	 * Returns a new object of class '<em>XML Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XML Element</em>'.
	 * @generated
	 */
	XMLElement createXMLElement();

	/**
	 * Returns a new object of class '<em>XML Entity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XML Entity</em>'.
	 * @generated
	 */
	XMLEntity createXMLEntity();

	/**
	 * Returns a new object of class '<em>XML Entity Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XML Entity Reference</em>'.
	 * @generated
	 */
	XMLEntityReference createXMLEntityReference();

	/**
	 * Returns a new object of class '<em>XML Notation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XML Notation</em>'.
	 * @generated
	 */
	XMLNotation createXMLNotation();

	/**
	 * Returns a new object of class '<em>XML Processing Instruction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XML Processing Instruction</em>'.
	 * @generated
	 */
	XMLProcessingInstruction createXMLProcessingInstruction();

	/**
	 * Returns a new object of class '<em>XML Text</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XML Text</em>'.
	 * @generated
	 */
	XMLText createXMLText();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	EXMLPackage getEXMLPackage();

} //EXMLFactory
