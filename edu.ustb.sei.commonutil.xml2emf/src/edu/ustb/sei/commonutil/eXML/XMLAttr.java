/**
 */
package edu.ustb.sei.commonutil.eXML;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XML Attr</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getNodeValue <em>Node Value</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getName <em>Name</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getValue <em>Value</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getLocalName <em>Local Name</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getPrefix <em>Prefix</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLAttr()
 * @model
 * @generated
 */
public interface XMLAttr extends XMLNode {
	/**
	 * Returns the value of the '<em><b>Node Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Value</em>' attribute.
	 * @see #setNodeValue(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLAttr_NodeValue()
	 * @model
	 * @generated
	 */
	String getNodeValue();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getNodeValue <em>Node Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Value</em>' attribute.
	 * @see #getNodeValue()
	 * @generated
	 */
	void setNodeValue(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLAttr_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLAttr_Value()
	 * @model
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Local Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Name</em>' attribute.
	 * @see #setLocalName(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLAttr_LocalName()
	 * @model
	 * @generated
	 */
	String getLocalName();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getLocalName <em>Local Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Name</em>' attribute.
	 * @see #getLocalName()
	 * @generated
	 */
	void setLocalName(String value);

	/**
	 * Returns the value of the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prefix</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prefix</em>' attribute.
	 * @see #setPrefix(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLAttr_Prefix()
	 * @model
	 * @generated
	 */
	String getPrefix();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLAttr#getPrefix <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prefix</em>' attribute.
	 * @see #getPrefix()
	 * @generated
	 */
	void setPrefix(String value);

} // XMLAttr
