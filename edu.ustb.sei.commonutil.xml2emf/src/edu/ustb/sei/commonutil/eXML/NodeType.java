/**
 */
package edu.ustb.sei.commonutil.eXML;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Node Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getNodeType()
 * @model
 * @generated
 */
public enum NodeType implements Enumerator {
	/**
	 * The '<em><b>Attribute node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ATTRIBUTE_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	ATTRIBUTE_NODE(0, "attribute_node", "attribute_node"), /**
	 * The '<em><b>Cdata section node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CDATA_SECTION_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	CDATA_SECTION_NODE(1, "cdata_section_node", "cdata_section_node"), /**
	 * The '<em><b>Comment node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMMENT_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	COMMENT_NODE(2, "comment_node", "comment_node"), /**
	 * The '<em><b>Document fragment node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOCUMENT_FRAGMENT_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	DOCUMENT_FRAGMENT_NODE(3, "document_fragment_node", "document_fragment_node"), /**
	 * The '<em><b>Document node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOCUMENT_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	DOCUMENT_NODE(4, "document_node", "document_node"), /**
	 * The '<em><b>Document type node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DOCUMENT_TYPE_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	DOCUMENT_TYPE_NODE(5, "document_type_node", "document_type_node"), /**
	 * The '<em><b>Element node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ELEMENT_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	ELEMENT_NODE(6, "element_node", "element_node"), /**
	 * The '<em><b>Entity node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENTITY_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	ENTITY_NODE(7, "entity_node", "entity_node"), /**
	 * The '<em><b>Entity reference node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ENTITY_REFERENCE_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	ENTITY_REFERENCE_NODE(8, "entity_reference_node", "entity_reference_node"), /**
	 * The '<em><b>Notation node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOTATION_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	NOTATION_NODE(9, "notation_node", "notation_node"), /**
	 * The '<em><b>Processing instruction node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROCESSING_INSTRUCTION_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	PROCESSING_INSTRUCTION_NODE(10, "processing_instruction_node", "processing_instruction_node"), /**
	 * The '<em><b>Text node</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEXT_NODE_VALUE
	 * @generated
	 * @ordered
	 */
	TEXT_NODE(11, "text_node", "text_node")
	;

	/**
	 * The '<em><b>Attribute node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Attribute node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ATTRIBUTE_NODE
	 * @model name="attribute_node"
	 * @generated
	 * @ordered
	 */
	public static final int ATTRIBUTE_NODE_VALUE = 0;

/**
	 * The '<em><b>Cdata section node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Cdata section node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CDATA_SECTION_NODE
	 * @model name="cdata_section_node"
	 * @generated
	 * @ordered
	 */
	public static final int CDATA_SECTION_NODE_VALUE = 1;

/**
	 * The '<em><b>Comment node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Comment node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMMENT_NODE
	 * @model name="comment_node"
	 * @generated
	 * @ordered
	 */
	public static final int COMMENT_NODE_VALUE = 2;

/**
	 * The '<em><b>Document fragment node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Document fragment node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DOCUMENT_FRAGMENT_NODE
	 * @model name="document_fragment_node"
	 * @generated
	 * @ordered
	 */
	public static final int DOCUMENT_FRAGMENT_NODE_VALUE = 3;

/**
	 * The '<em><b>Document node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Document node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DOCUMENT_NODE
	 * @model name="document_node"
	 * @generated
	 * @ordered
	 */
	public static final int DOCUMENT_NODE_VALUE = 4;

/**
	 * The '<em><b>Document type node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Document type node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DOCUMENT_TYPE_NODE
	 * @model name="document_type_node"
	 * @generated
	 * @ordered
	 */
	public static final int DOCUMENT_TYPE_NODE_VALUE = 5;

/**
	 * The '<em><b>Element node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Element node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ELEMENT_NODE
	 * @model name="element_node"
	 * @generated
	 * @ordered
	 */
	public static final int ELEMENT_NODE_VALUE = 6;

/**
	 * The '<em><b>Entity node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Entity node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ENTITY_NODE
	 * @model name="entity_node"
	 * @generated
	 * @ordered
	 */
	public static final int ENTITY_NODE_VALUE = 7;

/**
	 * The '<em><b>Entity reference node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Entity reference node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ENTITY_REFERENCE_NODE
	 * @model name="entity_reference_node"
	 * @generated
	 * @ordered
	 */
	public static final int ENTITY_REFERENCE_NODE_VALUE = 8;

/**
	 * The '<em><b>Notation node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Notation node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOTATION_NODE
	 * @model name="notation_node"
	 * @generated
	 * @ordered
	 */
	public static final int NOTATION_NODE_VALUE = 9;

/**
	 * The '<em><b>Processing instruction node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Processing instruction node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PROCESSING_INSTRUCTION_NODE
	 * @model name="processing_instruction_node"
	 * @generated
	 * @ordered
	 */
	public static final int PROCESSING_INSTRUCTION_NODE_VALUE = 10;

/**
	 * The '<em><b>Text node</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Text node</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TEXT_NODE
	 * @model name="text_node"
	 * @generated
	 * @ordered
	 */
	public static final int TEXT_NODE_VALUE = 11;

	/**
	 * An array of all the '<em><b>Node Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final NodeType[] VALUES_ARRAY =
		new NodeType[] {
			ATTRIBUTE_NODE,
			CDATA_SECTION_NODE,
			COMMENT_NODE,
			DOCUMENT_FRAGMENT_NODE,
			DOCUMENT_NODE,
			DOCUMENT_TYPE_NODE,
			ELEMENT_NODE,
			ENTITY_NODE,
			ENTITY_REFERENCE_NODE,
			NOTATION_NODE,
			PROCESSING_INSTRUCTION_NODE,
			TEXT_NODE,
		};

	/**
	 * A public read-only list of all the '<em><b>Node Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<NodeType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Node Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static NodeType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			NodeType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Node Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static NodeType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			NodeType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Node Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static NodeType get(int value) {
		switch (value) {
			case ATTRIBUTE_NODE_VALUE: return ATTRIBUTE_NODE;
			case CDATA_SECTION_NODE_VALUE: return CDATA_SECTION_NODE;
			case COMMENT_NODE_VALUE: return COMMENT_NODE;
			case DOCUMENT_FRAGMENT_NODE_VALUE: return DOCUMENT_FRAGMENT_NODE;
			case DOCUMENT_NODE_VALUE: return DOCUMENT_NODE;
			case DOCUMENT_TYPE_NODE_VALUE: return DOCUMENT_TYPE_NODE;
			case ELEMENT_NODE_VALUE: return ELEMENT_NODE;
			case ENTITY_NODE_VALUE: return ENTITY_NODE;
			case ENTITY_REFERENCE_NODE_VALUE: return ENTITY_REFERENCE_NODE;
			case NOTATION_NODE_VALUE: return NOTATION_NODE;
			case PROCESSING_INSTRUCTION_NODE_VALUE: return PROCESSING_INSTRUCTION_NODE;
			case TEXT_NODE_VALUE: return TEXT_NODE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private NodeType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //NodeType
