/**
 */
package edu.ustb.sei.commonutil.eXML;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XML Processing Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction#getNodeValue <em>Node Value</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction#getData <em>Data</em>}</li>
 *   <li>{@link edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLProcessingInstruction()
 * @model
 * @generated
 */
public interface XMLProcessingInstruction extends XMLNode {
	/**
	 * Returns the value of the '<em><b>Node Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node Value</em>' attribute.
	 * @see #setNodeValue(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLProcessingInstruction_NodeValue()
	 * @model
	 * @generated
	 */
	String getNodeValue();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction#getNodeValue <em>Node Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Value</em>' attribute.
	 * @see #getNodeValue()
	 * @generated
	 */
	void setNodeValue(String value);

	/**
	 * Returns the value of the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data</em>' attribute.
	 * @see #setData(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLProcessingInstruction_Data()
	 * @model
	 * @generated
	 */
	String getData();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction#getData <em>Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data</em>' attribute.
	 * @see #getData()
	 * @generated
	 */
	void setData(String value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' attribute.
	 * @see #setTarget(String)
	 * @see edu.ustb.sei.commonutil.eXML.EXMLPackage#getXMLProcessingInstruction_Target()
	 * @model
	 * @generated
	 */
	String getTarget();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.commonutil.eXML.XMLProcessingInstruction#getTarget <em>Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' attribute.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(String value);

} // XMLProcessingInstruction
