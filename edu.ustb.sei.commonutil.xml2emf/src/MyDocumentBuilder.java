import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class MyDocumentBuilder extends DocumentBuilder {
	private DocumentBuilder builder;
	
	public MyDocumentBuilder() throws ParserConfigurationException {
		builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	}

	@Override
	public DOMImplementation getDOMImplementation() {
		return builder.getDOMImplementation();
	}

	@Override
	public boolean isNamespaceAware() {
		return builder.isNamespaceAware();
	}

	@Override
	public boolean isValidating() {
		return builder.isValidating();
	}

	@Override
	public Document newDocument() {
		System.out.println("doc");
		return builder.newDocument();
	}

	@Override
	public Document parse(InputSource is) throws SAXException, IOException {
		return builder.parse(is);
	}

	@Override
	public void setEntityResolver(EntityResolver er) {
		builder.setEntityResolver(er);;
	}

	@Override
	public void setErrorHandler(ErrorHandler eh) {
		builder.setErrorHandler(eh);
	}

}
