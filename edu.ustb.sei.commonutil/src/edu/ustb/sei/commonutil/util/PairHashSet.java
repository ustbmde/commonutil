package edu.ustb.sei.commonutil.util;

import java.util.HashMap;
import java.util.HashSet;

public class PairHashSet<F,S> {
	private HashMap<F,HashSet<S>> set = new HashMap<F, HashSet<S>>();
	
	private HashSet<S> get(F f) {
		HashSet<S> s = set.get(f);
		if(s==null) {
			s = new HashSet<S>();
			set.put(f, s);
		}
		return s;
	}
	
	public boolean contains(F f,S s) {
		return get(f).contains(s);
	}
	
	public void remove(F f, S s) {
		get(f).remove(s);
	}
	
	public void remove(F f) {
		set.remove(f);
	}
	
	public void add(F f,S s) {
		get(f).add(s);
	}

	public boolean isEmpty() {
		return set.isEmpty();
	}

}
