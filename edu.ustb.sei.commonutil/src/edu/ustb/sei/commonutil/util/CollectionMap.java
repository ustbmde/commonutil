package edu.ustb.sei.commonutil.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CollectionMap<K,V> extends HashMap<K,List<V>>{
	private static final long serialVersionUID = 7521560512633948822L;

	public void putSingle(K key, V value) {
		List<V> list = get(key);
		if(list==null) {
			list = new ArrayList<V>();
			put(key,list);
		}
		list.add(value);
	}
}
