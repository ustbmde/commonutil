package edu.ustb.sei.commonutil.util;

import java.util.HashMap;
import java.util.Map.Entry;

public class PairHashMap<F, S, V> implements Cloneable {
	private HashMap<F, HashMap<S, V>> map = new HashMap<F, HashMap<S, V>>();
	
	public HashMap<F, HashMap<S, V>> getMap() {
		return map;
	}

	private HashMap<S,V> get(F f) {
		HashMap<S,V> subMap = map.get(f);
		if(subMap==null) {
			subMap = new HashMap<S,V>();
			map.put(f,subMap);
		}
		return subMap;
	}
	
	public V get(F f, S s) {
		return get(f).get(s);
	}
	
	public void put(F f, S s, V v) {
		get(f).put(s, v);
	}

	public void remove(F f) {
		map.remove(f);
	}
	
	public void remove(F f, S s) {
		get(f).remove(s);
	}
	
	public void reset() {
		map.clear();
	}
	
	public PairHashMap<F, S, V> clone() {
		PairHashMap<F, S, V> phm = new PairHashMap<F, S, V>();
		for (Entry<F, HashMap<S, V>> e : map.entrySet()) {
			if (e.getValue() == null)
				phm.map.put(e.getKey(), null);
			else {
				HashMap<S, V> value = new HashMap<S, V>();
				for (Entry<S, V> ie : e.getValue().entrySet()) {
					if (ie.getValue() == null)
						value.put(ie.getKey(), null);
					else
						value.put(ie.getKey(), ie.getValue());
				}
				phm.map.put(e.getKey(), value);
			}
		}
		return phm;
	}
}
