package edu.ustb.sei.commonutil.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class MultipleHashMap<F, D> {
	private HashMap<F,Collection<D>> map = new HashMap<F,Collection<D>>();
	private boolean bag;
	
	
	public MultipleHashMap() {
		this(true);
	}
	public MultipleHashMap(boolean bag) {
		super();
		this.bag = bag;
	}

	public void put(F f, D d) {
		get(f).add(d);
	}
	
	public Collection<D> get(F f) {
		Collection<D> col = map.get(f);
		if(col==null) {
			if(bag) {
				col = new ArrayList<D>();
			} else {
				col = new UniqueArrayList<D>();
			}
			map.put(f, col);
		}
		return col;
	}
	
	public void remove(F f) {
		map.remove(f);
	}
	
	public void remove(F f, D d) {
		get(f).remove(d);
	}
	
	public void reset() {
		map.clear();
	}

}
