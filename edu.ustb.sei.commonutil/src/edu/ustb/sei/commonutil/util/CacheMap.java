package edu.ustb.sei.commonutil.util;

import java.util.HashMap;

public class CacheMap {
	private HashMap<Object,Object> map;
	public CacheMap(){
		map = new HashMap<Object,Object>();
	}
	
	public void clear() {
		map.clear();
	}
	
	public <T> T getTypedValue(Class<? extends T> type, Object... keys) {
		@SuppressWarnings("unchecked")
		T v = (T)get(keys);
		return v;
	}
	
	@SuppressWarnings("unchecked")
	public Object get(Object... keys) {
		if(keys==null||keys.length==0) throw new RuntimeException("invalid keys in CacheMap");
		int s = keys.length-1;
		HashMap<Object,Object> cur = map;
		
		for(int i=0;i<s;i++){
			Object v = cur.get(keys[i]);
			if(v==null)
				return null;
			else if(!(v instanceof HashMap<?,?>)){
				throw new RuntimeException("invalid keys in CacheMap");
			} else {
				cur = (HashMap<Object,Object>)v;
			}
		}
		
		return cur.get(keys[s]);
	}
	
	@SuppressWarnings("unchecked")
	public void put(Object value, Object... keys) {
		if(keys==null||keys.length==0) throw new RuntimeException("invalid keys in CacheMap");
		int s = keys.length-1;
		HashMap<Object,Object> cur = map;
		for(int i=0;i<s;i++){
			Object v = cur.get(keys[i]);
			if(v==null)
				return;
			else if(!(v instanceof HashMap<?,?>)){
				throw new RuntimeException("invalid keys in CacheMap");
			} else {
				cur = (HashMap<Object,Object>)v;
			}
		}
		cur.put(keys[s], value);
	}
}
