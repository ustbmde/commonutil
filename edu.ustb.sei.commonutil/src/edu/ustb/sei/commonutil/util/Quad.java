package edu.ustb.sei.commonutil.util;

public class Quad<F, S, T, H> {
	private F first;
	private S second;
	private T third;
	private H forth;
	public F getFirst() {
		return first;
	}
	public void setFirst(F first) {
		this.first = first;
	}
	public S getSecond() {
		return second;
	}
	public void setSecond(S second) {
		this.second = second;
	}
	public T getThird() {
		return third;
	}
	public void setThird(T third) {
		this.third = third;
	}
	public H getForth() {
		return forth;
	}
	public void setForth(H forth) {
		this.forth = forth;
	}
	@Override
	public int hashCode() {
		final int prime = 8191;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((forth == null) ? 0 : forth.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		result = prime * result + ((third == null) ? 0 : third.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Quad other = (Quad) obj;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (forth == null) {
			if (other.forth != null)
				return false;
		} else if (!forth.equals(other.forth))
			return false;
		if (second == null) {
			if (other.second != null)
				return false;
		} else if (!second.equals(other.second))
			return false;
		if (third == null) {
			if (other.third != null)
				return false;
		} else if (!third.equals(other.third))
			return false;
		return true;
	}
}
