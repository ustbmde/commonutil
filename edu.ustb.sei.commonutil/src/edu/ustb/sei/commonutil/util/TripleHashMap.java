package edu.ustb.sei.commonutil.util;

import java.util.HashMap;
import java.util.Map.Entry;

public class TripleHashMap<F, S, T, O> implements Cloneable{
	private HashMap<F, HashMap<S, HashMap<T, O>>> map = new HashMap<F, HashMap<S, HashMap<T, O>>>();
	public HashMap<S,HashMap<T,O>> get(F f) {
		HashMap<S,HashMap<T,O>> subMap = map.get(f);
		if(subMap==null) {
			subMap = new HashMap<S,HashMap<T,O>>();
			map.put(f, subMap);
		}
		return subMap;
	}
	
	public HashMap<T,O> get(F f,S s) {
		HashMap<S, HashMap<T, O>> hashMap = get(f);
		HashMap<T,O> subMap = hashMap.get(s);
		if(subMap==null) {
			subMap = new HashMap<T,O>();
			hashMap.put(s, subMap);
		}
		return subMap;
	}
	
	public O get(F f, S s, T t) {
		return get(f,s).get(t);
	}
	
	public void put(F f, S s, T t, O o) {
		get(f,s).put(t, o);
	}
	
	public void remove(F f) {
		map.remove(f);
	}
	
	public void remove(F f, S s) {
		get(f).remove(s);
	}
	
	public void remove(F f, S s, T t) {
		get(f,s).remove(t);
	}
	
	public void reset() {
		map.clear();
	}
	
	public TripleHashMap<F, S, T, O> clone() {
		TripleHashMap<F, S, T, O> newMap = new TripleHashMap<F, S, T, O>();
		for (Entry<F, HashMap<S, HashMap<T, O>>> e1 : this.map.entrySet()) {
			if (e1.getValue() == null)
				newMap.map.put(e1.getKey(), null);
			else {
				HashMap<S, HashMap<T, O>> v2 = new HashMap<S, HashMap<T, O>>();
				for (Entry<S, HashMap<T, O>> e2 : e1.getValue().entrySet()) {
					if (e2.getValue() == null)
						v2.put(e2.getKey(), null);
					else {
						HashMap<T, O> v3 = new HashMap<T, O>();
						for (Entry<T, O> e3 : e2.getValue().entrySet()) {
							if (e3.getValue() == null)
								v3.put(e3.getKey(), null);
							else
								v3.put(e3.getKey(), e3.getValue());
						}
						v2.put(e2.getKey(), v3);
					}
				}
				newMap.map.put(e1.getKey(), v2);
			}
		}
		return newMap;
	}
}
