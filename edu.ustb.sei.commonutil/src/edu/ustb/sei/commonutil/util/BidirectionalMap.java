package edu.ustb.sei.commonutil.util;

import java.util.HashMap;
import java.util.Map.Entry;

/**
 * ע�⣬���಻���̰߳�ȫ��
 * @author He Xiao
 *
 * @param <Tf>
 * @param <Ts>
 */
public class BidirectionalMap <Tf,Ts> implements Cloneable{
	private HashMap<Tf,Pair<Tf,Ts>> forward;
	public HashMap<Tf, Pair<Tf, Ts>> getForward() {
		return forward;
	}

	public HashMap<Ts, Pair<Tf, Ts>> getBackward() {
		return backward;
	}

	private HashMap<Ts,Pair<Tf,Ts>> backward;
	private boolean consistency; // turning consistency on may reduce the performanc 

	public BidirectionalMap() {
		forward = new HashMap<Tf,Pair<Tf,Ts>>();
		backward = new HashMap<Ts,Pair<Tf,Ts>>();
		consistency = false;
	}
	
	static public <Tf, Ts> BidirectionalMap<Tf, Ts> createConsistentMap(){
		BidirectionalMap<Tf, Ts> map = new BidirectionalMap<Tf, Ts>();
		map.consistency = true;
		return map;
	}
	
	public void put(Tf first, Ts second) {
		if(this.consistency) {
			if(first==null && second==null) return;
			else if(first==null) {
				Pair<Tf,Ts> p = backward.get(second);
				if(p!=null) {
					forward.remove(p.getFirst());
					backward.remove(p.getSecond());
				}
			} else if(second==null) {
				Pair<Tf,Ts> p = forward.get(first);
				if(p!=null) {
					forward.remove(p.getFirst());
					backward.remove(p.getSecond());
				}
			} else {
				this.removeForward(first);
				this.removeBackward(second);
				Pair<Tf,Ts> pair = new Pair<Tf,Ts>(first,second);
				forward.put(first, pair);
				backward.put(second, pair);
			}
		} else {
			if(first==null && second==null) return;
			else if(first==null) {
				Pair<Tf,Ts> p = backward.get(second);
				if(p!=null) {
					forward.remove(p.getFirst());
					backward.remove(p.getSecond());
				}
			} else if(second==null) {
				Pair<Tf,Ts> p = forward.get(first);
				if(p!=null) {
					forward.remove(p.getFirst());
					backward.remove(p.getSecond());
				}
			} else {
				Pair<Tf,Ts> pair = new Pair<Tf,Ts>(first,second);
				forward.put(first, pair);
				backward.put(second, pair);
			}
		}
	}
	
	public Ts forward(Tf f) {
		Pair<Tf,Ts> pair = forward.get(f);
		if(pair==null) return null;
		else return pair.getSecond();
	}
	
	public Tf backward(Ts s) {
		Pair<Tf,Ts> pair = backward.get(s);
		if(pair==null) return null;
		else return pair.getFirst();
	}

	public void removeForward(Tf f) {
		Pair<Tf,Ts> pair = forward.get(f);
		if (pair != null) {
			forward.remove(f);
			backward.remove(pair.getSecond());
		}
	}
	
	public void removeBackward(Ts s) {
		Pair<Tf,Ts> pair = backward.get(s);
		if (pair != null) {
			backward.remove(s);
			forward.remove(pair.getFirst());
		}
	}
	
	public void clear() {
		forward.clear();
		backward.clear();
	}
	
	public BidirectionalMap<Tf, Ts> clone() {
		BidirectionalMap<Tf, Ts> map = new BidirectionalMap<Tf, Ts>();
		map.consistency = this.consistency;
		
		for(Entry<Tf,Pair<Tf,Ts>> e : this.forward.entrySet()) {
			map.forward.put(e.getKey(), e.getValue());
		}
		
		for(Entry<Ts,Pair<Tf,Ts>> e : this.backward.entrySet()) {
			map.backward.put(e.getKey(), e.getValue());
		}
		
		return map;
	}
}
