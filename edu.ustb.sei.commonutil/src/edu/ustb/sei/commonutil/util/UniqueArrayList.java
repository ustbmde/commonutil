package edu.ustb.sei.commonutil.util;

import java.util.ArrayList;
import java.util.Collection;

public class UniqueArrayList<E> extends ArrayList<E> {
	private static final long serialVersionUID = 4837891753516074659L;

	public UniqueArrayList() {
		
	}

	public UniqueArrayList(int initialCapacity) {
		super(initialCapacity);
		
	}

	public UniqueArrayList(Collection<? extends E> c) {
		super(c);
		
	}
	
	public boolean add(E e) {
		if(this.contains(e)) return false;
		return super.add(e);
	}

	@Override
	public void add(int index, E element) {
		if(this.contains(element)) return;
		super.add(index, element);
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		c.retainAll(this);
		return super.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		c.retainAll(this);
		return super.addAll(index, c);
	}	
}
