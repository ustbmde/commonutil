package edu.ustb.sei.commonutil.serialization;

import java.util.concurrent.ConcurrentHashMap;

import edu.ustb.sei.commonutil.serialization.xml.AbstractJavaXMLConvertor;

public class ValueConvertorFactory {
	static private ValueConvertorFactory SINGLETON = new ValueConvertorFactory();
	static public ValueConvertorFactory getInstance() {return SINGLETON;}
	
	public ValueConvertorFactory() {
		super();
		initDefaultValueConvertor();
	}
	
	protected void initDefaultValueConvertor() {
		registerValueConvertorForClass(String.class, PrimitiveValueConvertor.class);
		registerValueConvertorForClass(Integer.class, PrimitiveValueConvertor.class);
		registerValueConvertorForClass(Long.class, PrimitiveValueConvertor.class);
		registerValueConvertorForClass(Float.class, PrimitiveValueConvertor.class);
		registerValueConvertorForClass(Double.class, PrimitiveValueConvertor.class);
		registerValueConvertorForClass(Byte.class, PrimitiveValueConvertor.class);
		registerValueConvertorForClass(Character.class, PrimitiveValueConvertor.class);
		registerValueConvertorForClass(Boolean.class, PrimitiveValueConvertor.class);
	}
	

	private ConcurrentHashMap<Class<? extends IValueConvertor>, IValueConvertor> valueConvertorMap = new ConcurrentHashMap<Class<? extends IValueConvertor>, IValueConvertor>();
	public IValueConvertor getValueConvertor(Class<? extends IValueConvertor> cls) {
		IValueConvertor convertor = valueConvertorMap.get(cls);
		if(convertor==null) {
			try {
				convertor = cls.newInstance();
				valueConvertorMap.put(cls, convertor);
			} catch (InstantiationException | IllegalAccessException e) {
				convertor = null;
				e.printStackTrace();
				// log
			}
		}
		return convertor;
	}
	
	
	
	private ConcurrentHashMap<Class<?>, Class<? extends IValueConvertor>> valueConvertorRegistry = new ConcurrentHashMap<Class<?>, Class<? extends IValueConvertor>>();
	public IValueConvertor queryValueConvertorForClass(Class<?> type) {
		Class<? extends IValueConvertor> vc = valueConvertorRegistry.get(type);
		if(vc == null) {
			// log
			return null;
		} else {
			return getValueConvertor(vc);
		}
	}
	
	public void registerValueConvertorForClass(Class<?> type, Class<? extends IValueConvertor> defaultConvertor) {
		valueConvertorRegistry.put(type, defaultConvertor);
	}
	
	/*private ConcurrentHashMap<Class<? extends AbstractXMLElementConvertor>, AbstractXMLElementConvertor> xmlElementConvertorMap = 
			new ConcurrentHashMap<Class<? extends AbstractXMLElementConvertor>, AbstractXMLElementConvertor>();
	public AbstractXMLElementConvertor getXMLElementConvertor(Class<? extends AbstractXMLElementConvertor> cls) {
		AbstractXMLElementConvertor convertor = xmlElementConvertorMap.get(cls);
		if(convertor==null) {
			try {
				convertor = cls.newInstance();
				xmlElementConvertorMap.put(cls, convertor);
			} catch (InstantiationException | IllegalAccessException e) {
				convertor = null;
				e.printStackTrace();
				// log
			}
		}
		return convertor;
	}
	
	private ConcurrentHashMap<Class<?>, Class<? extends AbstractXMLElementConvertor>> xmlElementConvertorRegistry = new ConcurrentHashMap<Class<?>, Class<? extends AbstractXMLElementConvertor>>();
	public AbstractXMLElementConvertor queryXMLElementConvertorForClass(Class<?> type) {
		Class<? extends AbstractXMLElementConvertor> vc = xmlElementConvertorRegistry.get(type);
		if(vc == null) {
			// log
			return null;
		} else {
			return getXMLElementConvertor(vc);
		}
	}*/
	
	
	
	
}
