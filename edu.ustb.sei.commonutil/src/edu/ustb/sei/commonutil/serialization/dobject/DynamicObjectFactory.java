package edu.ustb.sei.commonutil.serialization.dobject;

import java.util.Set;

public class DynamicObjectFactory {
	static private void dObjectToString(DynamicObject o, StringBuilder builder) {
		builder.append('{');
		boolean isFirst = true;
		
		Set<String> keys = o.keySet();
		for(String k : keys) {
			Object v = o.get(k);
			if(v!=null) {
				if(isFirst) isFirst=false;
				else builder.append(',');
				builder.append('\"');
				builder.append(k);
				builder.append('\"');
				builder.append(':');
				
				convertObject(v, builder);
			}
		}
		
		builder.append('}');
	}
	private static void convertObject(Object v, StringBuilder builder) {
		if(v instanceof String) {
			builder.append('\"');
			builder.append(v);
			builder.append('\"');
		} else if(v instanceof Integer || v instanceof Double || v instanceof Float) {
			builder.append(v.toString());
		} else if(v instanceof Boolean) {
			builder.append(v.toString());
		} else if(v instanceof DynamicObject) {
			dObjectToString((DynamicObject)v, builder);
		} else if(v instanceof DynamicList) {
			dListToString((DynamicList)v, builder);
		}
	}
	
	static private void dListToString(DynamicList l, StringBuilder builder) {
		builder.append('[');
		boolean isFirst = true;
		for(Object o : l) {
			if(isFirst) isFirst = false;
			else builder.append(',');
			if(o instanceof IDynamicObject) 
				dObjectToString((DynamicObject) o, builder);
			else if(o instanceof DynamicList)
				dListToString((DynamicList) o, builder);
			else
				convertObject(o,builder);
		}
		
		builder.append(']');
	}
	
	static public String toString(Object o) {
		StringBuilder builder = new StringBuilder();
		convertObject(o,builder);
		return builder.toString();
	}

}
