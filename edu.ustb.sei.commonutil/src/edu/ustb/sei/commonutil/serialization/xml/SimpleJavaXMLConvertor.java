package edu.ustb.sei.commonutil.serialization.xml;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.ustb.sei.commonutil.serialization.IValueConvertor;
import edu.ustb.sei.commonutil.serialization.annotation.AnnotationCollector;
import edu.ustb.sei.commonutil.serialization.annotation.DataContainmentReference;
import edu.ustb.sei.commonutil.serialization.annotation.DataCrossReference;
import edu.ustb.sei.commonutil.serialization.annotation.DataProperty;
import edu.ustb.sei.commonutil.serialization.annotation.MultiplicityType;
import edu.ustb.sei.commonutil.util.Pair;
import edu.ustb.sei.commonutil.util.Triple;

public class SimpleJavaXMLConvertor extends AbstractJavaXMLConvertor {

	protected Element createOrUpdateXMLElement(Class<?> type, Element refNode, Document document) {
		Element element = null;
		
		if(refNode == null) {
			element = document.createElement(type.getName());
		} else {
			element = document.createElement(type.getName());
		}
		
		return element;
	}

	protected void saveXMLAttribute(Object o, Class<?> type, Element refNode,Document document) {
		Field[] properties = AnnotationCollector.getAllDataProperties(type);
		
		for(Field f : properties) {
			Attr attr = document.createAttribute(f.getName());
			Object value = null;
			//boolean flag = f.isAccessible();
			try {
				//f.setAccessible(true);
				value = f.get(o);
				
				if(value!=null) {
					
					DataProperty a = f.getAnnotation(DataProperty.class);
					
					
					IValueConvertor vc = getValueConvertorFactory().getValueConvertor(a.dataConvertor());
					if(a.multiplicity()==MultiplicityType.single) {
						attr.setValue(vc.serialize(value).toString());
					} else if(a.multiplicity() == MultiplicityType.array) {
						StringBuilder buf = new StringBuilder();
						buf.append("[");
						int len = Array.getLength(value);
						
						for(int i=0;i<len;i++) {
							if(i>0) buf.append(",");
							Object it = Array.get(value, i);
							buf.append(vc.serialize(it));
						}
						buf.append("]");
						attr.setValue(buf.toString());
					} 
					refNode.setAttributeNode(attr);
					
				}
			} catch (IllegalArgumentException | IllegalAccessException | SecurityException |NullPointerException e) {
				// TODO Auto-generated catch block
				// log
				e.printStackTrace();
			}
			//f.setAccessible(flag);
		}
	}

	@SuppressWarnings("unchecked")
	protected void loadXMLAttribute(Object obj, Class<?> targetClass,
			Element currentNode, Document document) {
		Field[] properties =  AnnotationCollector.getAllDataProperties(targetClass);
		
		for(Field f : properties) {
			DataProperty d = f.getAnnotation(DataProperty.class);
			
			String xmlValue = currentNode.getAttribute(f.getName());
			if(xmlValue==null||xmlValue.isEmpty()) continue;
			else {
				try {
					Object actualValue = null;
					
					IValueConvertor valueConvertor = getValueConvertorFactory().getValueConvertor(d.dataConvertor());
					
					if(d.multiplicity()==MultiplicityType.single) {
						actualValue = valueConvertor.deserialize(xmlValue, d.elementType());
					} else if(d.multiplicity() == MultiplicityType.array) {
						String buf[] = xmlValue.substring(1, xmlValue.length()-1).split("\\[|,|\\]");
						actualValue = Array.newInstance(d.elementType(), buf.length);
						for(int i=0;i<buf.length;i++) {
							Array.set(actualValue, i, valueConvertor.deserialize(buf[i], d.elementType()));
						}
					}

					//boolean accFlag = f.isAccessible();
					//f.setAccessible(true);
					f.set(obj, actualValue);
					//f.setAccessible(accFlag);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	protected void saveXMLContainmentReference(Object o, Class<?> type,
			Element currentNode, Document document) {
		
		Field[] properties =  AnnotationCollector.getAllDataContainmentReferences(type);
		
		for(Field f : properties) {			
			try {
				//boolean accFlag = f.isAccessible();
				//f.setAccessible(true);
				Object elementObject = f.get(o);
				if(elementObject!=null) {
					DataContainmentReference d = f.getAnnotation(DataContainmentReference.class);
					
					if(d.multiplicity()==MultiplicityType.list) {
						Element element = document.createElement(f.getName());
						currentNode.appendChild(element);
						@SuppressWarnings("rawtypes")
						List list = (List) elementObject;
						
						for(Object lo : list) {
							Element newElement = serialize(lo, element, document);
							element.appendChild(newElement);
						}
						
					} else if(d.multiplicity()==MultiplicityType.single) {
						Element element = document.createElement(f.getName());
						currentNode.appendChild(element);
						Object lo = elementObject;
						Element newElement = serialize(lo, element, document);
						element.appendChild(newElement);
					}
				
				}
				
				//f.setAccessible(accFlag);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void loadXMLContainmentReference(Object obj,
			Class<?> targetClass, Element currentNode, Document document) {
		
		Field[] properties =  AnnotationCollector.getAllDataContainmentReferences(targetClass);
		for(Field f : properties) {
			DataContainmentReference d = f.getAnnotation(DataContainmentReference.class);
			try {
				//boolean accFlag = f.isAccessible();
				//f.setAccessible(true);
				if(d.multiplicity()==MultiplicityType.list) {
					List<Element> children = getChildrenByNameNS(currentNode, f.getName(), null);
					if(children.size()!=1) continue;
					List list = d.collectionType().newInstance();
					Element referenceRoot = children.get(0);
					//NodeList childNodes = referenceRoot.getChildNodes();
					
					Node currentChild = referenceRoot.getFirstChild();
					
					while(currentChild!=null){
						//Element ce = (Element)childNodes.item(i);
						Object e = deserialize((Element) currentChild, document);
						if(e!=null) list.add(e);
						currentChild = currentChild.getNextSibling();
					}
					
					f.set(obj, list);
					
				} else if(d.multiplicity()==MultiplicityType.single) {
					List<Element> children = getChildrenByNameNS(currentNode, f.getName(), null);
					if(children.size()!=1) continue;
					Element referenceRoot = children.get(0);
					Element ce = (Element)referenceRoot.getFirstChild();
					Object e = deserialize(ce, document);
					if(e!=null) f.set(obj, e);
				}
				
				//f.setAccessible(accFlag);
			} catch (IllegalArgumentException | IllegalAccessException | InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@Override
	protected void saveXMLCrossReference(Object o, Class<?> targetClass, Element currentNode, Document document) {
		
		Field[] properties =  AnnotationCollector.getAllDataCrossReferences(targetClass);
		for(Field f : properties) {
			try{
				Object elementObject = f.get(o);
				if(elementObject==null) continue;
				
				DataCrossReference d = f.getAnnotation(DataCrossReference.class);
				if(d.multiplicity()==MultiplicityType.list) {
					Element listElement = document.createElement(f.getName());
					currentNode.appendChild(listElement);
					
					@SuppressWarnings("rawtypes")
					List list = (List) elementObject;
					
					for(Object lo : list) {
						//Element refElement = obj2element.forward(lo);
						Element newElement = document.createElement(lo.getClass().getName());
						listElement.appendChild(newElement);							
						//if(refElement==null){
							Triple<Element,Field,Object> triple = new Triple<Element,Field,Object>(newElement,f,lo);
							
							objCrossRefs.add(triple);
						//} else {
						//	newElement.setAttributeNS(XLINK, "xlink:href", collectPath(refElement,document));
						//}
					}
				} else if(d.multiplicity()==MultiplicityType.single) {
					Element listElement = document.createElement(f.getName());
					currentNode.appendChild(listElement);
					//Element refElement = obj2element.forward(elementObject);
					Element newElement = document.createElement(elementObject.getClass().getName());
					listElement.appendChild(newElement);
					//if(refElement==null){
					Triple<Element,Field,Object> triple = new Triple<Element,Field,Object>(newElement,f,elementObject);
					objCrossRefs.add(triple);
					//} else {
					//	newElement.setAttributeNS(XLINK, "xlink:href", collectPath(refElement,document));
					//}
				}
				
			} catch(IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void completeSavingXMLCrossReference(Document document) {
		for(Triple<Element,Field,Object> triple : objCrossRefs) {
			Element refElement = obj2element.forward(triple.getThird());
			triple.getFirst().setAttributeNS(XLINK, "xlink:href", collectPath(refElement,document));
		}	
	}

	@Override
	protected void loadXMLCrossReference(Object obj, Class<?> targetClass,
			Element currentNode, Document document) {
		Field[] properties =  AnnotationCollector.getAllDataCrossReferences(targetClass);
		for(Field f : properties) {
			DataCrossReference d = f.getAnnotation(DataCrossReference.class);
			if(d.multiplicity()==MultiplicityType.list||d.multiplicity()==MultiplicityType.single) {
				List<Element> list = getChildrenByNameNS(currentNode, f.getName(), null);
				for(Element le : list) {
					Node cur = le.getFirstChild();
					while(cur!=null) {
						String path = ((Element)cur).getAttributeNS(XLINK, "href");
						if(path==null) continue;
						Element pe = locateElement(path, document);
						if(pe==null) continue;
						Triple<Object,Field,Element> tri = new Triple<Object,Field,Element>(obj,f,pe);
						this.elementCrossRefs.add(tri);
						cur = cur.getNextSibling();
					}
				}
			}
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void completeLoadingXMLCrossReference(Document document) {
		
		for(Triple<Object,Field,Element> t : elementCrossRefs) {
			DataCrossReference d = t.getSecond().getAnnotation(DataCrossReference.class);
			try {
				Object currentValue = t.getSecond().get(t.getFirst());
				Object po = obj2element.backward(t.getThird());
				if(d.multiplicity()==MultiplicityType.list) {
					if(currentValue==null){
						List list = d.collectionType().newInstance();
						list.add(po);
						t.getSecond().set(t.getFirst(), list);
					} else if(currentValue instanceof List) {
						((List)currentValue).add(po);
					}
				} else if(d.multiplicity()==MultiplicityType.single) {
					t.getSecond().set(t.getFirst(), po);
				}
			} catch (IllegalArgumentException | IllegalAccessException | InstantiationException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected String getClassType(Element element) {
		return element.getTagName();
	}
	
	

}
