package edu.ustb.sei.commonutil.serialization.xml;

import java.lang.reflect.Field;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import edu.ustb.sei.commonutil.serialization.annotation.AnnotationCollector;
import edu.ustb.sei.commonutil.serialization.annotation.DataContainmentReference;
import edu.ustb.sei.commonutil.serialization.annotation.DataCrossReference;
import edu.ustb.sei.commonutil.serialization.annotation.MultiplicityType;
import edu.ustb.sei.commonutil.util.Triple;

public class JavaXMIConvertor extends SimpleJavaXMLConvertor {
	protected Element createOrUpdateXMLElement(Class<?> type, Element refNode, Document document) {
		Element element = null;
		
		if(refNode == null) {
			element = document.createElement(type.getName());//root element
		} else {
			//element = document.createElement(type.getName());
			refNode.setAttributeNS(XSINS, "xsi:type", type.getName());
			element = refNode;
		}
		
		return element;
	}
	
	@Override
	protected void saveXMLContainmentReference(Object o, Class<?> type,
			Element currentNode, Document document) {
		
		Field[] properties =  AnnotationCollector.getAllDataContainmentReferences(type);
		
		for(Field f : properties) {			
			try {
				//boolean accFlag = f.isAccessible();
				//f.setAccessible(true);
				Object elementObject = f.get(o);
				if(elementObject!=null) {
					DataContainmentReference d = f.getAnnotation(DataContainmentReference.class);
					
					if(d.multiplicity()==MultiplicityType.list) {
						@SuppressWarnings("rawtypes")
						List list = (List) elementObject;						
						for(Object lo : list) {
							Element element = document.createElement(f.getName());
							currentNode.appendChild(element);
							Element newElement = serialize(lo, element, document);
							assert element==newElement;
						}
						
					} else if(d.multiplicity()==MultiplicityType.single) {
						Element element = document.createElement(f.getName());
						currentNode.appendChild(element);
						Object lo = elementObject;
						Element newElement = serialize(lo, element, document);
						assert element==newElement;
					}
				
				}
				
				//f.setAccessible(accFlag);
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void loadXMLContainmentReference(Object obj,
			Class<?> targetClass, Element currentNode, Document document) {
		
		Field[] properties =  AnnotationCollector.getAllDataContainmentReferences(targetClass);
		for(Field f : properties) {
			DataContainmentReference d = f.getAnnotation(DataContainmentReference.class);
			try {
				//boolean accFlag = f.isAccessible();
				//f.setAccessible(true);
				if(d.multiplicity()==MultiplicityType.list) {
					List<Element> children = getChildrenByNameNS(currentNode, f.getName(), null);
					if(children.size()==0) continue;
					
					List list = d.collectionType().newInstance();
					
					for(Element currentChild : children) {
						Object e = deserialize((Element) currentChild, document);
						if(e!=null) list.add(e);
					}
					
//					Element referenceRoot = children.get(0);
//					//NodeList childNodes = referenceRoot.getChildNodes();
//					
//					Node currentChild = referenceRoot.getFirstChild();
//					
//					while(currentChild!=null){
//						//Element ce = (Element)childNodes.item(i);
//						Object e = deserialize((Element) currentChild, document);
//						if(e!=null) list.add(e);
//						currentChild = currentChild.getNextSibling();
//					}
					
					f.set(obj, list);
					
				} else if(d.multiplicity()==MultiplicityType.single) {
					List<Element> children = getChildrenByNameNS(currentNode, f.getName(), null);
					if(children.size()==0) continue;
					Element single = children.get(0);
					//Element ce = (Element)referenceRoot.getFirstChild();
					Object e = deserialize(single, document);
					if(e!=null) f.set(obj, e);
				}
				
				//f.setAccessible(accFlag);
			} catch (IllegalArgumentException | IllegalAccessException | InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	@Override
	protected String getClassType(Element element) {
		String xsitype = element.getAttributeNS(XSINS, "type");
		if(xsitype==null||xsitype.length()==0)
			return element.getTagName();
		else return xsitype;
	}
	
	@Override
	protected void saveXMLCrossReference(Object o, Class<?> targetClass, Element currentNode, Document document) {
		
		Field[] properties =  AnnotationCollector.getAllDataCrossReferences(targetClass);
		for(Field f : properties) {
			try{
				Object elementObject = f.get(o);
				if(elementObject==null) continue;
				
				DataCrossReference d = f.getAnnotation(DataCrossReference.class);
				if(d.multiplicity()==MultiplicityType.list) {
					@SuppressWarnings("rawtypes")
					List list = (List) elementObject;
					
					for(Object lo : list) {
						//Element listElement = document.createElement(f.getName());
						//currentNode.appendChild(listElement);
						
						//Element refElement = obj2element.forward(lo);
//						Element newElement = document.createElement(lo.getClass().getName());
//						listElement.appendChild(newElement);							
						//if(refElement==null){
						Triple<Element,Field,Object> triple = new Triple<Element,Field,Object>(currentNode,f,lo);
						objCrossRefs.add(triple);
						//} else {
						//	newElement.setAttributeNS(XLINK, "xlink:href", collectPath(refElement,document));
						//}
					}
				} else if(d.multiplicity()==MultiplicityType.single) {
					//Element listElement = document.createElement(f.getName());
					//currentNode.appendChild(listElement);
					//Element refElement = obj2element.forward(elementObject);
					//Element newElement = document.createElement(elementObject.getClass().getName());
					//listElement.appendChild(newElement);
					//if(refElement==null){
					Triple<Element,Field,Object> triple = new Triple<Element,Field,Object>(currentNode,f,elementObject);
					objCrossRefs.add(triple);
					//} else {
					//	newElement.setAttributeNS(XLINK, "xlink:href", collectPath(refElement,document));
					//}
				}
				
			} catch(IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void completeSavingXMLCrossReference(Document document) {
		for(Triple<Element,Field,Object> triple : objCrossRefs) {
			Element refElement = obj2element.forward(triple.getThird());
			DataCrossReference d = triple.getSecond().getAnnotation(DataCrossReference.class);
			String path = collectPath(refElement,document);
			if(d.multiplicity()==MultiplicityType.list) {
				String value = triple.getFirst().getAttribute(triple.getSecond().getName());
				if(value==null||value.length()==0) value = path;
				else value = value + " " + path;
				//Node n = document.createTextNode(path);
				//triple.getFirst().appendChild(n);
				triple.getFirst().setAttribute(triple.getSecond().getName(), value);
			} else {
				triple.getFirst().setAttribute(triple.getSecond().getName(), path);
			}
		}	
	}
	
	@Override
	protected void loadXMLCrossReference(Object obj, Class<?> targetClass,
			Element currentNode, Document document) {
		Field[] properties =  AnnotationCollector.getAllDataCrossReferences(targetClass);
		for(Field f : properties) {
			DataCrossReference d = f.getAnnotation(DataCrossReference.class);
			if(d.multiplicity()==MultiplicityType.list) {
				//List<Element> list = getChildrenByNameNS(currentNode, f.getName(), null);
				String paths = currentNode.getAttribute(f.getName());
				if(paths==null || paths.length()==0) continue;
				String buf[] = paths.split(" ");
				
				for(String path : buf) {
//					String path = le.getNodeValue();
//					if(path==null) continue;
//					Element pe = locateElement(path, document);
//					if(pe==null) continue;
//					Triple<Object,Field,Element> tri = new Triple<Object,Field,Element>(obj,f,pe);
//					this.elementCrossRefs.add(tri);
					if(path==null) continue;
					Element pe = locateElement(path, document);
					if(pe==null) continue;
					Triple<Object,Field,Element> tri = new Triple<Object,Field,Element>(obj,f,pe);
					this.elementCrossRefs.add(tri);
				}
			} else if(d.multiplicity()==MultiplicityType.single) {
				String path = currentNode.getAttribute(f.getName());
				if(path==null) continue;
				Element pe = locateElement(path, document);
				if(pe==null) continue;
				Triple<Object,Field,Element> tri = new Triple<Object,Field,Element>(obj,f,pe);
				this.elementCrossRefs.add(tri);
			}
		}
	}
}
