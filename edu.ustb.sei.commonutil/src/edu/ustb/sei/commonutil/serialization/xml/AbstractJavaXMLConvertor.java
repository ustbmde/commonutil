package edu.ustb.sei.commonutil.serialization.xml;

import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.ustb.sei.commonutil.serialization.ValueConvertorFactory;
import edu.ustb.sei.commonutil.serialization.annotation.DataElement;
import edu.ustb.sei.commonutil.util.BidirectionalMap;
import edu.ustb.sei.commonutil.util.CollectionMap;
import edu.ustb.sei.commonutil.util.Pair;
import edu.ustb.sei.commonutil.util.Triple;

public abstract class AbstractJavaXMLConvertor {
	private ValueConvertorFactory valueConvertorFactory;
	protected BidirectionalMap<Object, Element> obj2element;
	protected List<Triple<Element,Field,Object>> objCrossRefs; // element.field = object
	protected List<Triple<Object,Field,Element>> elementCrossRefs; // object.field = element
	protected DocumentBuilderFactory builderFactory;
	protected TransformerFactory transformerFactory;
	
	
	public void reset() {
		obj2element.clear();
		objCrossRefs.clear();
		elementCrossRefs.clear();
	}
	public AbstractJavaXMLConvertor() {
		setValueConvertorFactory(ValueConvertorFactory.getInstance());
		obj2element = new BidirectionalMap<Object, Element>();
		objCrossRefs = new ArrayList<Triple<Element,Field,Object>>();
		elementCrossRefs = new ArrayList<Triple<Object,Field,Element>>();
		builderFactory =DocumentBuilderFactory.newInstance();
		transformerFactory = TransformerFactory.newInstance();
	}
	
	public void serializeRoot(Object o, Document document) {
		if(o instanceof List) {
			List<?> list = (List<?>)o;
			Element listElement = document.createElementNS(JDKNS, "jdk:List");
			registerDefaultNamespace(listElement);
			document.appendChild(listElement);
			
			for(Object lo : list) {
				Element element = serialize(lo,null,document);
				//registerDefaultNamespace(element);
				listElement.appendChild(element);
			}
		} else {
			Element element = serialize(o,null,document);
			registerDefaultNamespace(element);
			document.appendChild(element);
		}
		
		completeSavingXMLCrossReference(document);
	}
	
	private void registerDefaultNamespace(Element e) {
		e.setAttribute("xmlns:xlink", XLINK);
		e.setAttribute("xmlns:xsi", XSINS);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object deserializeRoot(Document document) {
		Element root = document.getDocumentElement();
		
		if(root==null) return null;
		
		if(JDKNS.equals(root.getNamespaceURI())) {
			if(root.getLocalName().equals("List")) {
				List list = new ArrayList();
				int size = root.getChildNodes().getLength();
				for(int i=0;i<size;i++) {
					Element e = (Element) root.getChildNodes().item(i);
					Object o = deserialize(e, document);
					list.add(o);
				}
				completeLoadingXMLCrossReference(document);
				return list;
			}
		} else {
			Object o = deserialize(root, document);
			completeLoadingXMLCrossReference(document);
			return o;
		}
		
		return null;
	}
	
	public Element serialize(Object o, Element refNode, Document document) {
		Class<?> type = o.getClass();
		DataElement elementAnnotation = type.getAnnotation(DataElement.class);
		if(elementAnnotation==null) return null;
		
		Element element = createOrUpdateXMLElement(type, refNode, document);
		obj2element.put(o, element);
		
		saveXMLAttribute(o, type, element, document);
		saveXMLContainmentReference(o, type, element, document);
		saveXMLCrossReference(o, type, element, document);

		return element;
	}	
	
	public Object deserialize(Element currentNode, Document document) {
		if(currentNode==null) {
			currentNode = document.getDocumentElement();
		}
		String className = getClassType(currentNode);
		
		try {
			Class<?> targetClass = Class.forName(className);
			Object obj = targetClass.newInstance();
			
			obj2element.put(obj, currentNode);
			
			loadXMLAttribute(obj,targetClass,currentNode,document);
			loadXMLContainmentReference(obj, targetClass, currentNode, document);
			loadXMLCrossReference(obj, targetClass, currentNode, document);
			return obj;
			
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	abstract protected String getClassType(Element element);
	abstract protected Element createOrUpdateXMLElement(Class<?> type, Element refNode, Document document);
	abstract protected void saveXMLAttribute(Object o, Class<?> type, Element refNode,Document document);
	abstract protected void saveXMLContainmentReference(Object o, Class<?> type, Element refNode,Document document);
	abstract protected void saveXMLCrossReference(Object o, Class<?> type, Element refNode,Document document);
	abstract protected void completeSavingXMLCrossReference(Document document);
	abstract protected void loadXMLAttribute(Object obj, Class<?> targetClass, Element currentNode, Document document);
	abstract protected void loadXMLContainmentReference(Object obj,Class<?> targetClass, Element currentNode, Document document);
	abstract protected void loadXMLCrossReference(Object obj,Class<?> targetClass, Element currentNode, Document document);
	abstract protected void completeLoadingXMLCrossReference(Document document);
	
	static public List<Element> getChildrenByNameNS(Element parent, String name, String namespace) {
		List<Element> list = new ArrayList<Element>();
		//NodeList children = parent.getChildNodes();
		//int i,s=children.getLength();
		
		if(namespace==null){
			Node cur = parent.getFirstChild();
			while(cur!=null) {
				Element element = (Element)cur;
				if(element.getTagName().equals(name))
					list.add(element);
				cur = cur.getNextSibling();
			}
		} else {
			Node cur = parent.getFirstChild();
			while(cur!=null) {
				Element element = (Element)cur;
				if(namespace.equals(element.getNamespaceURI()) && name.equals(element.getLocalName()))
					list.add(element);
				cur = cur.getNextSibling();
			}
		}
		
		return list;
	}
	
	public ValueConvertorFactory getValueConvertorFactory() {
		return valueConvertorFactory;
	}

	public void setValueConvertorFactory(ValueConvertorFactory valueConvertorFactory) {
		this.valueConvertorFactory = valueConvertorFactory;
	}
	
	public Document createXMLDocument() throws ParserConfigurationException {
		Document document;
		DocumentBuilderFactory builderFactory=DocumentBuilderFactory.newInstance();
		document = builderFactory.newDocumentBuilder().newDocument();
		document.setXmlVersion("1.0");
		
		return document;
	}
	
	
	public void saveXMLDocument(Document document, OutputStream out) throws TransformerException {
		Transformer t = transformerFactory.newTransformer();
		t.setOutputProperty(OutputKeys.ENCODING, "GB2312");
		t.setOutputProperty(OutputKeys.INDENT, "yes");//�����Զ�����
		t.setOutputProperty(OutputKeys.METHOD, "xml");//�������������ķ�����
		DOMSource ds = new DOMSource(document);
		StreamResult result = new StreamResult(out);
		t.transform(ds, result);
	}
	
	static public String collectPath(Node e, Document d) {
		Node parent =  e.getParentNode();
		Node cur = e;
		
		List<Pair<String,Integer>> buf = new ArrayList<Pair<String,Integer>>();
		
		
		while(parent!=null) {
			int i = getElementIndexGroupByName(parent,cur);
//			Node pt = parent.getFirstChild();
//			while(pt!=null&&pt!=cur) {
//				pt = pt.getNextSibling();
//				i++;
//			}
			buf.add(new Pair<String,Integer>(cur.getNodeName(),i));
			cur = parent;
			parent = parent.getParentNode();
		}
		
		StringBuilder builder = new StringBuilder();
		builder.append("#/");
		for(int i=buf.size()-1;i>=0;i--) {
			builder.append("/@");
			Pair<String,Integer> p = buf.get(i);
			builder.append(p.getFirst());
			builder.append('[');
			builder.append(p.getSecond());
			builder.append(']');
		}
		return builder.toString();
	}
	
	static public int getElementIndexGroupByName(Node parent, Node child) {
		int i=-1;
		Node cur = parent.getFirstChild();
		while(cur!=null) {
			if(cur.getNodeName().equals(child.getNodeName())
					//&& cur.getNamespaceURI().equals(child.getNamespaceURI())
					) 
				i++;
			if(cur==child) break;
		}
		return i;
	}
	
	static public Node getElementByIndexGroupByName(Node parent,String path){
		String buf[] = path.split("\\[|\\]");
		if(buf.length==2) {
			String localname = buf[0];
			int i = Integer.parseInt(buf[1]);
			int j = -1;
			Node pt = parent.getFirstChild();
			while(pt!=null) {
				if(pt.getNodeName().equals(localname))
					j++;
				if(j==i) return pt;
			}
			return null;
		} else {
			return null;
		}
	}
	
	static public Element locateElement(String path, Document d) {
		 String[] buf = path.split("/@");
		 if(buf[0].equals("#/")) {
			 Node parent = d;
			 Node cur = null;
			 int i=1;
			 while(parent!=null&&i<buf.length) {
//				 NodeList childNodes = parent.getChildNodes();
//				 if(childNodes==null) {
//					 parent = null;
//					 cur=null;
//				 } else {
//					 cur = childNodes.item(Integer.parseInt(buf[i]));
//					 parent = cur;
//				 }
				 cur = getElementByIndexGroupByName(parent,buf[i]);
				 parent = cur;
				 i++;
			 }
			 return (Element) cur;
		 } else {
			 return null;
		 }
	}
	
	

	static final public String XSINS = "http://www.w3.org/2001/XMLSchema-instance";
	static final public String XLINK = "http://www.w3.org/1999/xlink";
	static final public String JDKNS = "java.util";
}
