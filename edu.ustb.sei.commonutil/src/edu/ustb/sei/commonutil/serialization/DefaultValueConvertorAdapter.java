package edu.ustb.sei.commonutil.serialization;

/**
 * @author He Xiao
 * 该类仅是一个代理，它会查询ConvertorFactory注册的Convertor进行实际操作
 */
public class DefaultValueConvertorAdapter implements IValueConvertor {

	@Override
	public Object serialize(Object o) {
		Class<?> cls = o.getClass();
		IValueConvertor vc = ValueConvertorFactory.getInstance().queryValueConvertorForClass(cls);
		if(vc==null) {
			// log
			return null;
		} else {
			return vc.serialize(o);
		}
	}

	@Override
	public Object deserialize(Object o, Class<?> targetType) {
		IValueConvertor vc = ValueConvertorFactory.getInstance().queryValueConvertorForClass(targetType);
		if(vc==null) {
			// log
			return null;
		}
		else 
			return vc.deserialize(o, targetType);
	}

}
