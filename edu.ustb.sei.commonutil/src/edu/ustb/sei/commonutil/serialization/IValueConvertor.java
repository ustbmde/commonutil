package edu.ustb.sei.commonutil.serialization;

/**
 * 将一个对象转换成存储格式
 * @author He Xiao
 *
 */
public interface IValueConvertor {
	Object serialize(Object o);
	Object deserialize(Object o, Class<?> targetType);
}
