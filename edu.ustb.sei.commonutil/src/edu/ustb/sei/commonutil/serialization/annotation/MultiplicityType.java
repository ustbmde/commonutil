package edu.ustb.sei.commonutil.serialization.annotation;

public enum MultiplicityType {
	single,
	list,
	array,
	map
}
