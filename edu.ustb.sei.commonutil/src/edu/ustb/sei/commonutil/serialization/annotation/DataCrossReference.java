package edu.ustb.sei.commonutil.serialization.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataCrossReference {
	/**
	 * ֻ֧��list��single
	 * @return
	 */
	MultiplicityType multiplicity() default MultiplicityType.list;
	
	@SuppressWarnings("rawtypes")
	Class<? extends List> collectionType() default ArrayList.class;
}
