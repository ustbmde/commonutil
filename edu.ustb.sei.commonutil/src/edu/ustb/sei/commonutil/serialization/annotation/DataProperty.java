package edu.ustb.sei.commonutil.serialization.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collection;

import edu.ustb.sei.commonutil.serialization.DefaultValueConvertorAdapter;
import edu.ustb.sei.commonutil.serialization.IValueConvertor;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataProperty {
	/**
	 * 只支持array和single
	 * @return
	 */
	MultiplicityType multiplicity() default MultiplicityType.single;
	
	Class<?> elementType();
	
	/**
	 * 值的转换器，默认支持基本类型
	 * 扩展值转换器有两种方法，一种是使用dataConvertor设置值转换器，另一种方法是在ConvertorFactory中注册新的转换器
	 * @return
	 */
	Class<? extends IValueConvertor> dataConvertor() default DefaultValueConvertorAdapter.class;
	
	Class<?> collectionType() default Object.class; 
}
