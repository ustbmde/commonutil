package edu.ustb.sei.commonutil.serialization.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class AnnotationCollector {
	

	private static final Field[] EMPTY_FIELDS = new Field[0];
	
	static private Field[] getAllAnnotatedFields(Class<?> cls,ConcurrentHashMap<Class<?>, Field[]> map, Class<? extends Annotation> annotationType) {
		Annotation an = cls.getAnnotation(DataElement.class);
		if(an==null)
			return null;
		
		Field[] result = map.get(cls);
		
		if(result == null) {
			int totalSize = 0;
			List<Field[]> allProperties  = new ArrayList<Field[]>();
			
			Class<?> parent = cls.getSuperclass();
			
			while(parent!=null){
				Field[] superAn = null;
				superAn = getAllAnnotatedFields(parent,map,annotationType);
				if(superAn==null) break;
				allProperties.add(superAn);
				totalSize += superAn.length;
				parent = parent.getSuperclass();
			}
			
			List<Field> current = new ArrayList<Field>();
			for(Field f : cls.getDeclaredFields()) {
				if(f.getAnnotation(annotationType)!=null)
					current.add(f);
			}
			allProperties.add(current.toArray(EMPTY_FIELDS));
			totalSize += current.size();
			
			result = new Field[totalSize];
			for(int i = 0;i<totalSize;) {
				for(Field[] pp : allProperties) {
					for(int j=0;j<pp.length;i++,j++)
						result[i] = pp[j];
				}
			}
			
			map.put(cls, result);
		}
		return result;
	}
	
	static private ConcurrentHashMap<Class<?>, Field[]> dataPropertyMap = new ConcurrentHashMap<Class<?>, Field[]>();
	static public Field[] getAllDataProperties(Class<?> cls) {
//		Annotation an = cls.getAnnotation(DataElement.class);
//		if(an==null)
//			return null;
//		
//		Field[] result = dataPropertyMap.get(cls);
//		
//		if(result == null) {
//			int totalSize = 0;
//			List<Field[]> allProperties  = new ArrayList<Field[]>();
//			
//			Class<?> parent = cls.getSuperclass();
//			
//			while(parent!=null){
//				Field[] superAn = null;
//				superAn = getAllDataProperties(parent);
//				if(superAn==null) break;
//				allProperties.add(superAn);
//				totalSize += superAn.length;
//				parent = parent.getSuperclass();
//			}
//			
//			List<Field> current = new ArrayList<Field>();
//			for(Field f : cls.getDeclaredFields()) {
//				if(f.getAnnotation(DataProperty.class)!=null)
//					current.add(f);
//			}
//			allProperties.add(current.toArray(EMPTY_FIELDS));
//			totalSize += current.size();
//			
//			result = new Field[totalSize];
//			for(int i = 0;i<totalSize;) {
//				for(Field[] pp : allProperties) {
//					for(int j=0;j<pp.length;i++,j++)
//						result[i] = pp[j];
//				}
//			}
//			
//			dataPropertyMap.put(cls, result);
//		}
//		return result;
		return getAllAnnotatedFields(cls, dataPropertyMap, DataProperty.class);
		
	}
	
	static private ConcurrentHashMap<Class<?>, Field[]> dataContainmentReferenceMap = new ConcurrentHashMap<Class<?>, Field[]>();
	static public Field[] getAllDataContainmentReferences(Class<?> cls) {
		return getAllAnnotatedFields(cls, dataContainmentReferenceMap, DataContainmentReference.class);
	}
	
	static private ConcurrentHashMap<Class<?>, Field[]> dataCrossReferenceMap = new ConcurrentHashMap<Class<?>, Field[]>();
	static public Field[] getAllDataCrossReferences(Class<?> cls) {
		return getAllAnnotatedFields(cls, dataCrossReferenceMap, DataCrossReference.class);
	}
}
