package edu.ustb.sei.commonutil.serialization;

public class PrimitiveValueConvertor implements IValueConvertor {

	@Override
	public Object serialize(Object o) {
		
		if(o==null)
			return null;
		
		Class<?> type = o.getClass();
		if(type==String.class) {
			return o;
		} else if(type==Integer.class||type==Long.class
				||type==Double.class||type==Float.class
				||type==Byte.class||type==Character.class
				||type==Boolean.class) {
			return o.toString();
		} else {
			// log
			return null;
		}
	}

	@Override
	public Object deserialize(Object o, Class<?> targetType) {
		
		
		String str = (String)o;
		if(str==null) return null;
		if(str=="" && targetType!=String.class) return null;
		
		if(targetType==String.class)
			return str;
		else if(targetType==Integer.class)
			return Integer.parseInt(str);
		else if(targetType==Long.class)
			return Long.parseLong(str);
		else if(targetType==Double.class)
			return Double.parseDouble(str);
		else if(targetType==Float.class)
			return Float.parseFloat(str);
		else if(targetType==Byte.class)
			return Byte.parseByte(str);
		else if(targetType==Character.class)
			return str.charAt(0);
		else if(targetType==Boolean.class)
			return Boolean.parseBoolean(str);
		else {
			// log
			return null;
		}
	}

}
