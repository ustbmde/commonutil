import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import edu.ustb.sei.commonutil.serialization.xml.AbstractJavaXMLConvertor;
import edu.ustb.sei.commonutil.serialization.xml.JavaXMIConvertor;
import edu.ustb.sei.commonutil.serialization.xml.SimpleJavaXMLConvertor;


public class Test {

	public static void main(String[] args) {
		
		
		
		MyData d1 = new MyData();
		d1.name="AAA";
		d1.sex = true;
		d1.address = new String[]{"<�����Ƽ���ѧ","����������ѧ"};

		MyData d2 = new MyData();
		d2.name="BBB";
		d2.sex = false;
		d1.upper=d2;

		MyData d3 = new MyData();
		d3.name="CCC";
		d3.sex = true;
		d2.upper = d3;

		MyData d4 = new MyData();
		d4.name="DDD";
		d4.sex = true;
		d3.upper = d4;
		
		d4.myRef = d3;
		d1.myRef=d4;

		
		List<Object> list = new ArrayList<Object>();
		
		//d1.upper = d2;
		d1.children = list;
		list.add(d3);
		list.add(d4);
				
		
		AbstractJavaXMLConvertor convertor = new SimpleJavaXMLConvertor();
		
		DocumentBuilderFactory builderFactory=DocumentBuilderFactory.newInstance();
		Document document;
		try {
			document = builderFactory.newDocumentBuilder().newDocument();
			
			document.setXmlVersion("1.0");
			//document.setXmlStandalone(true);
			
			convertor.serializeRoot(d1, document);
			
			
			document.normalizeDocument();
			
			
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.ENCODING, "GB2312");
			t.setOutputProperty(OutputKeys.INDENT, "yes");//�����Զ�����
			t.setOutputProperty(OutputKeys.METHOD, "xml");//�������������ķ�����
			//t.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "namespaceAnalysisReport.dtd");//����<!DOCTYPE��system����ֵ��
			
			DOMSource ds = new DOMSource(document);
			StreamResult result = new StreamResult(System.out);
			t.transform(ds, result);
			
			convertor.reset();
			
			@SuppressWarnings("unused")
			Object o = convertor.deserializeRoot(document);
			
			assert false;

		} catch (ParserConfigurationException | TransformerFactoryConfigurationError | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}