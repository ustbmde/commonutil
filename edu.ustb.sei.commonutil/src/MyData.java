
import java.util.List;

import edu.ustb.sei.commonutil.serialization.annotation.DataContainmentReference;
import edu.ustb.sei.commonutil.serialization.annotation.DataCrossReference;
import edu.ustb.sei.commonutil.serialization.annotation.DataElement;
import edu.ustb.sei.commonutil.serialization.annotation.DataProperty;
import edu.ustb.sei.commonutil.serialization.annotation.MultiplicityType;


@DataElement
public class MyData {
	@DataProperty(elementType = String.class)
	public String name;
	
	@DataProperty(elementType = Boolean.class)
	public boolean sex;
	
	@DataProperty(elementType = String.class,multiplicity=MultiplicityType.array)
	public String[] address;
	
	@DataContainmentReference(multiplicity=MultiplicityType.single)
	public MyData upper;
	
	@DataCrossReference(multiplicity=MultiplicityType.list)
	public List children;
	
	@DataCrossReference(multiplicity=MultiplicityType.single)
	public MyData myRef;
}